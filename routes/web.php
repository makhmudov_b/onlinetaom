<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Frontend
// Route::get('categorie', 'PageController@categories');
// Route::get('events', 'PageController@events');

// Route::get('basket', 'PageController@basket');
// Route::get('history', 'PageController@history');

// Route::get('mark', 'PageController@mark');
// Route::get('info', 'PageController@info');
// Route::get('verify', 'PageController@verify');
// Route::get('number', 'PageController@number');

// Route::get('search', 'PageController@search');
// Route::get('profile', 'PageController@profile');
// Route::get('filter', 'PageController@filter');
// Route::get('payment/method', 'PageController@method');
// Route::get('favorite/rest', 'PageController@rest');
// Route::get('favorite/food', 'PageController@food');
// Route::get('restaurant/inner', 'PageController@inner');
//Route::get('restaurant/description', 'PageController@description');


//Backend

Route::group(['middleware' => ['role:admin','web'] ], function () {
    Route::get('restaurants/create', 'RestaurantController@create');

    Route::get('orders/{id}/review', 'ReviewController@index');
    Route::get('/order/review', 'ReviewController@show');
    Route::get('orders/{id}/review/create', 'ReviewController@create');
    Route::get('orders/{id}/review/edit', 'ReviewController@edit');

    Route::get('restaurant/{id}/promo', 'PromoController@index');
    Route::get('restaurant/{id}/promo/create', 'PromoController@create');
    Route::get('restaurant/{id}/promo/edit', 'PromoController@edit');

    Route::get('categories/create', 'CategoryController@create');
    Route::get('categories/{id}', 'CategoryController@show');
    Route::get('categories', 'CategoryController@index');
    Route::get('categories/{id}/edit', 'CategoryController@edit');

    Route::get('districts/create', 'DistrictController@create');
    Route::get('districts/{id}', 'DistrictController@show');
    Route::get('districts/', 'DistrictController@index');
    Route::get('districts/{id}/edit', 'DistrictController@edit');

    Route::get('users', 'UserController@index');
    Route::get('managers', 'UserController@manager');
    Route::get('operators', 'UserController@callcenter');
    Route::get('moderators', 'UserController@moderator');

    Route::get('carriers', 'CarrierController@index');
    Route::get('carriers/{id}/orders', 'CarrierController@orders');

    Route::get('user/{id}/order', 'UserController@orders');

    //      User request
    Route::get('users/create', 'UserController@create');
    Route::get('users/edit/{id}', 'UserController@edit');

    Route::post('user', 'UserController@store');
    Route::put('user/{id}', 'UserController@update');
    Route::delete('user/{id}', 'UserController@delete');


    //      Carrier request
    Route::get('carriers/create', 'CarrierController@create');
    Route::get('carriers/edit/{id}', 'CarrierController@edit');
    Route::get('carriers/order/edit/{id}', 'CarrierController@editOrder');

    Route::post('carrier', 'CarrierController@store');
    Route::put('carrier/{id}', 'CarrierController@update');
    Route::put('carrier/order/{id}', 'CarrierController@updateOrder');
    Route::delete('carrier/{id}/delete', 'CarrierController@delete');
});

Route::group(['middleware' => ['role:admin,manager,callcenter','web'] ], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/notify', function(){
        return view('managment');
    });
    Route::get('favorite', 'FavoriteController@index');
    Route::get('restaurants', 'RestaurantController@index');

    Route::get('orders', 'OrderController@index');
    Route::get('orders/by/{id}', 'OrderController@getOrderBy');
    Route::patch('orders/between', 'OrderController@getBetween');
    Route::get('orders/{id}', 'OrderController@showInner');
    Route::put('orders/{id}', 'OrderController@successOrder');
    Route::put('orders/ignore/{id}', 'OrderController@ignoreOrder');
    Route::delete('orders/{id}', 'OrderController@deleteOrder');

    Route::get('food', 'FoodController@index');

    Route::get('restaurant/{id}/menu', 'FoodController@menu');

    Route::get('food/create', 'FoodController@create');
    Route::get('food/edit/{id}', 'FoodController@edit');

    Route::get('restaurants/{id}/edit', 'RestaurantController@edit');
    Route::get('restaurant/{id}/schedule', 'ScheduleController@index');
    Route::get('restaurant/{id}/schedule/create', 'ScheduleController@create');
    Route::get('restaurant/{id}/schedule/edit', 'ScheduleController@edit');
    Route::get('food/{id}/params', 'FoodController@params');
    Route::get('food/{id}/params/create', 'ParamController@create');
    Route::get('food/{id}/params/edit', 'ParamController@edit');

    Route::get('food/{id}/size', 'FoodController@size');
    Route::get('food/{id}/size/create', 'SizeController@create');
    Route::get('food/{id}/size/edit', 'SizeController@edit');

    Route::post('notifications', 'NotificationController@store');
    Route::get('notifications', 'NotificationController@index');
    Route::get('import', 'OrderController@import');
    Route::patch('notifications/{id}/read', 'NotificationController@markAsRead');
    Route::post('notifications/mark-all-read', 'NotificationController@markAllRead');
    Route::post('notifications/{id}/dismiss', 'NotificationController@dismiss');

    // Push Subscriptions
    Route::post('subscriptions', 'PushSubscriptionController@update');
    Route::post('subscriptions/delete', 'PushSubscriptionController@destroy');

    Route::post('/food/{id}/size', 'SizeController@store');
    Route::put('/food/{id}/size', 'SizeController@update');
    Route::delete('/food/{id}/size', 'SizeController@delete');

    Route::post('/food/{id}/params', 'ParamController@store');
    Route::put('/food/{id}/params', 'ParamController@update');
    Route::delete('/food/{id}/params', 'ParamController@delete');

    Route::post('/districts', 'DistrictController@store');
    Route::put('/districts/{id}', 'DistrictController@update');
    Route::delete('/districts/{id}', 'DistrictController@delete');

    Route::post('/categories', 'CategoryController@store');
    Route::put('/categories/{id}', 'CategoryController@update');
    Route::delete('/categories/{id}', 'CategoryController@delete');

    Route::patch('/food/schedule', 'ScheduleController@update');

    Route::post('/promo/{id}', 'PromoController@store');
    Route::put('/promo/update/{id}', 'PromoController@update');
    Route::delete('/promo/delete/{id}', 'PromoController@delete');

    Route::post('/restaurant', 'RestaurantController@store');
    Route::post('/restaurant/{id}', 'RestaurantController@update');

    Route::delete('/restaurant/delete/{id}', 'RestaurantController@delete');
    Route::put('/restaurant/restore/{id}', 'RestaurantController@restore');

    Route::post('/food', 'FoodController@store');
    Route::put('/food/{id}', 'FoodController@update');
    Route::delete('/food/{id}', 'FoodController@delete');
    Route::put('/food/restore/{id}', 'FoodController@restore');
});


Auth::routes();
Route::get('/{path}', 'PageController@index')->where('path', '.*');
