<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promo extends Model
{
    use SoftDeletes;    
    protected $guarded = [];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image'];

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }
    /**
     * Get an image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return env('APP_URL').'/uploads/promo/'.$this->id.'.png';
    }
}
