<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Schedule extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    /**
     * The schedule that belong to the restaurant.
     *
     * @return BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }
}