<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\User;
use App\Order;
use App\Role;

class AuthController extends Controller {
    public function register (Request $request)
    {
        $request['password'] = Hash::make($request['password']);
        $user = User::create($request->toArray());
        $token = $user->createToken($user->name);

        $response = ['token' => $token];

        return response($response, 200);
    }
    public function check (Request $request)
    {
        $password = rand(1000, 9999);
        $phone = (int) $request->phone;
        $user = User::where(['phone' => $phone])->first();
        if ($user === null){
            $user = User::create([
                'name' => 'Пользователь',
                'email' => 'Не задан '.$password,
                'phone' => $phone,
                'password' => bcrypt($password)
            ]);
            $role = Role::where('type', 'member')->first();
            $user->roles()->attach($role);
        }
        else{
            $user->update([
                'password' => bcrypt($password)
            ]);
        }

        $client = new Client();
        $response = $client->post( 'http://91.204.239.42:8083/broker-api/send', [
            'auth' => ['onlinetaom', 'c6ppTR7'],

            'json' => [
                    "messages" => [
                        [
                            "recipient" => 998 . $user->phone,
                                    "message-id"=>"abc".$password.$password,

                            "sms"=> [
                                "originator"=> "3700",
                                "content"=> ["text" => "Ваш код подтверждения: ". $password.'. Наберите его в поле ввода.']
                                ]
                            ]
                    ]
            ]
        ]);

        return response()->json($user, 200);
    }

    public function login (Request $request)
    {
        $user = User::where('phone', $request->phone)->first();
        $order = Order::where('user_id' , $user->id)->latest()->first();

        if (Hash::check($request->password, $user->password)) {
            $token = $user->createToken($user->phone);
            $token->expires_at = Carbon::now()->addWeeks(1);
            if($order == null || !$order ){
                Order::create([
                    'long'=> 1,
                    'lat'=> 1,
                    'adress'=> 'SomeAdress',
                    'distance'=> 'Calculated Distance',
                    'device_info'=> 'DeviceInfo',
                    'payment_method'=> 1,
                    'status'=> 0,
                    'shipping_time'=> 0,
                    'shipping_price'=> 0,
                    'total_price'=> 0,
                    'user_id'=>$user->id,
                    'restaurant_id'=> 0,
                ]);
            }
            return response($token, 200);
        } else { $response = "Password missmatch";
            return response($response, 422);
        }
    }
    public function restLogin (Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $rest = $user->restaurant;
        if (Hash::check($request->password, $user->password) && !is_null($rest)) {
                $token = $user->createToken($user->email);
                $token->expires_at = Carbon::now()->addWeeks(1);
            return response($token, 200);
        } else {
            $response = "Password missmatch";
            return response($response, 422);
        }
    }
    public function logout (Request $request)
    {
        $request->user()->token()->revoke();
        $response = 'You have been succesfully logged out!';
        return response($response, 200);
    }
}
