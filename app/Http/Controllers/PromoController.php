<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Promo;
use App\Restaurant;
use Carbon\Carbon;
use Intervention\Image\ImageManagerStatic as Image;

class PromoController extends Controller
{
    public function index($id)
    {
        $data = Restaurant::where('alias', $id)->first()->promos;
        return view('backend.promo.index',compact('id','data'));
    }
    public function show()
    {
        $data = Promo::where('slide','1')->with(['restaurant','restaurant.categories'])->take(3)->get();
        return response()->json($data);
    }
    public function get()
    {
        $data = Promo::with(['restaurant','restaurant.categories'])->get();
        return response()->json($data);
    }
    public function create($id)
    {
        return view('backend.promo.create',compact('id'));
    }    
    public function edit($id)
    {
        $data = Promo::find($id);
        return view('backend.promo.edit',compact('data','id'));
    }
    public function store(Request $request, $id)
    {
        request()->validate([
            'start_time' => 'required',
            'end_time' => 'required',
            'title_uz' => 'required',
            'title_ru' => 'required',
        ]);         
        $rest = Restaurant::where('alias',$id)->first();

        $promo = Promo::create([
            'start_time'=>request('start_time'),
            'end_time'=>request('end_time'),
            'title_uz'=>request('title_uz'),
            'title_ru'=>request('title_ru'),
            'slide'=> request('slide') || 0,
            'restaurant_id'=> $rest->id,
        ]);
        $image = $request->file('image');
        $filename = $promo->id .'.png' ;
		$path = public_path('uploads/promo/'. $filename);
        Image::make($image->getRealPath())->encode('png')
        ->save($path);

        return redirect()->action('PromoController@index',$promo->restaurant->alias)->with('success','Успешно добавлено');
    }
    public function update(Request $request, $id)
    {
        $promo = Promo::findOrFail($id);
        $promo->update([
            'start_time'=>request('start_time'),
            'end_time'=>request('end_time'),
            'title_uz'=>request('title_uz'),
            'title_ru'=>request('title_ru'),
            'slide'=> request('slide') || 0,
        ]);
        if( $request->file('image') ){
            $old = Image::make('uploads/promo/'.$id.'.png');
            $old->destroy();
            $image = $request->file('image');
            $filename = $promo->id.'.png' ;
            $path = public_path('uploads/promo/'. $filename);
            Image::make($image->getRealPath())->encode('png', 90)
            ->save($path);
        }
        return redirect()->action('PromoController@index',$promo->restaurant->alias)->with('success','Успешно изменен');
    }
    public function delete($id)
    {
        $promo = Promo::findOrFail($id);
        $old = Image::make('uploads/promo/'.$id.'.png');
        $old->destroy();
        $promo->delete();
        return redirect()->back()->with('success','Успешно удален');
    }
}
