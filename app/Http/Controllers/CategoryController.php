<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Category;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
    public function index()
    {
        $data = Category::all();
        return view('backend.categories.index',compact('data'));
    }
    public function indexJson()
    {
        $data = Category::take(9)->get();
        return response()->json($data);
    }
    public function showBy($alias)
    {
        $data = Category::where('alias', $alias)->with('restaurants.categories')->get();
        return response()->json($data);
    }
    public function show($id)
    {
        $data = Category::find($id);
        return response()->json($data);
    }

    public function create()
    {
        return view('backend.categories.create');
    }
    public function edit($id)
    {
        $data = Category::find($id);
        return view('backend.categories.edit', compact('data','id'));
    }

    /**
     * Store Category
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
        ]);

        $data = Category::create($request->all());

        $image = $request->file('image');
        $filename = $data->id .'.png' ;
		$path = public_path('uploads/categories/'. $filename);

        Image::make($image->getRealPath())->encode('png')->resize(200,200)
        ->save($path);

        return redirect()->action('CategoryController@index')->with('success','Успешно добавлен');
    }
    /**
     * Delete Category
     * @param  $id
     * @return Response
     */
    public function delete($id)
    {
        $data = Category::find($id);
        $data->delete();
        return redirect()->action('CategoryController@index')->with('success','Успешно удален');
    }
    /**
     * update Category
     * @param Request $request, $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'name_uz' => 'required',
            'name_ru' => 'required',
        ]);
        Category::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('CategoryController@index')->with('success','Успешно изменен');
    }
}
