<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Param;
use App\Food;
use Intervention\Image\ImageManagerStatic as Image;

class ParamController extends Controller
{
    public function create($id)
    {
        return view('backend.params.create',compact('id'));
    }
    public function edit($id)
    {
        $data = Param::find($id);
        return view('backend.params.edit',compact('data','id'));
    }
    public function show($id)
    {
        $data = Food::findOrFail($id)->params;
        return $data;
    }

    public function store(Request $request, $id)
    {
        request()->validate([
            'name' => 'required',
            'price' => 'required',
        ]);
        $param = Param::create([
            'name'=>request('name'),
            'price'=>request('price'),
            'food_id'=>request('id')
        ]);
        $image = $request->file('image');
        $filename = $param->id .'.jpg' ;
		$path = public_path('uploads/params/'. $filename);
        Image::make($image->getRealPath())->encode('jpg', 90)
        ->crop(200, 200)
        ->save($path);
        return redirect()->action('FoodController@params',$param->food->id)->with('success','Успешно добавлено');;
    }
    public function update(Request $request, $id)
    {
        $param = Param::findOrFail($id);
        if ($request->file('image')) {
            $old = Image::make('uploads/params/' . $id . '.jpg');
            $old->destroy();
            $image = $request->file('image');
            $filename = $param->id .'.jpg' ;
            $path = public_path('uploads/params/'. $filename);
            Image::make($image->getRealPath())->encode('jpg', 90)
            ->crop(200, 200)
            ->save($path);
        }

        $param->update([
            'name'=>request('name'),
            'price'=>request('price')
        ]);
        return redirect()->action('FoodController@params',$param->food->id)->with('success','Успешно изменен');
    }
    public function delete($id)
    {
        $param = Param::findOrFail($id);
        $food_id = $param->food->id;
        $param->delete();
        return redirect()->action('FoodController@params',$food_id)->with('success','Успешно удален');
    }
}
