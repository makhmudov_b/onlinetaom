<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\District;

class DistrictController extends Controller
{
    public function index()
    {
        $data = District::all();
        return view('backend.districts.index',compact('data'));
    }
    public function show($id)
    {
        $data = District::find($id);
        return response()->json($data);
    }    
    
    public function create()
    {
        return view('backend.districts.create');
    }
    public function edit($id)
    {
        $data = District::find($id);
        return view('backend.districts.edit', compact('data','id'));
    }

    /**
     * Store District
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name_uz' => 'required',
            'name_ru' => 'required'
        ]);
        $data = District::create($request->all());
        return redirect()->action('DistrictController@index')->with('success','Успешно добавлено');
    }
    /**
     * Delete District
     * @param  $id
     * @return Response
     */    
    public function delete($id)
    {
        $data = District::find($id);
        $data->delete();
        return redirect()->action('DistrictController@index')->with('success','Успешно удален');
    }
    /**
     * update District
     * @param Request $request, $id
     * @return Response
     */       
    public function update(Request $request, $id)
    {
        $data = District::findOrFail($id)->update(
            $request->all()
        );
        return redirect()->action('DistrictController@index')->with('success','Успешно изменен');
    }    
}
