<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Restaurant;
use App\Food;
use GuzzleHttp\Client;

class LocationController extends Controller
{
    public function getData()
    {
        $data = app('geocoder')->reverse(request('lat'),request('long'))->toJson();
//        $data = \GoogleMaps::load('geocoding')
//		->setParam ([
//            'latlng' => request('lat') . ','.request('long'),
//            'accuracy'           => 0,
//            "language"           => "uz-UZ",
//        ])
// 		->get('results.formatted_address');
        return $data;
    }
    public function search()
    {
        $data[0] = Restaurant::where('name', 'like', '%'.request('keyword').'%')->orWhere('desc_ru', 'like', '%'.request('keyword').'%')->with(['categories','foods'])->get();
        $data[1] = Food::has('restaurant')->where('name_ru', 'like', '%'.request('keyword').'%')->with(['restaurant','restaurant.categories'])->get();
        collect($data);
        return response()->json($data);
    }

    public function distance()
    {
        $rest = Restaurant::where('id',request('restaurantId'))->first();
        $main = $rest->ot_delta * 1;
        $secondary = $rest->delivery_delta * 1;
        $summ = $main + $secondary;
        $response = \GoogleMaps::load('directions')
                ->setParam([
                    'origin'          => request('origin'),
                    'destination'     => request('destination'),
                    'travel_mode'     => 'DRIVING',
                    "language"           => "ru-RU",
                ])
       ->get();
        $data =  json_decode($response)->routes[0]->legs[0];
        $distance = $data->distance->text;
        $time = $data->duration->text;
        $distance_km = 1 * filter_var($distance, FILTER_SANITIZE_NUMBER_INT);
        $shipping_price_admin = 8000;
            if($distance_km < 40){
            $price =  number_format($shipping_price_admin - $summ,0,'.',' ');
        }
        else{
            $price =  number_format(round(( $distance_km - 40) * 180 + $shipping_price_admin - $summ, -2),0,'.',' ');
        }
        return response()->json(['distance' => $distance,'price' => $price, 'time' => $time]);
    }

    public function payme()
    {
        $data = request()->all();
        return $data;
    }
}
