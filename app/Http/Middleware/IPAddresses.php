<?php

namespace App\Http\Middleware;

use Closure;

class IPAddresses
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        // if (!in_array($request->ip(),['81.95.231.206'])) {
        //     abort(403);
        // }

        return $next($request);
    }
}
