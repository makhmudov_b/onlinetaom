<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable
{
    use SoftDeletes ,Notifiable , HasApiTokens , HasPushSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'adress', 'password','restaurant_id','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = ['id', 'name','adress','email','last_visit','phone','token'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
    public function orders()
    {
        return $this->hasMany('App\Order')->withTrashed();
    }
    public function favorites()
    {
        return $this->hasMany('App\Favorite');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant')->withTrashed();
    }
    public function foodIds()
    {
        return $this->restaurant->foods->pluck('id');
    }
    public function hasRole($role)
    {
        return $this->roles()->where('type', $role)->first();
    }
}
