<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

use App\Restaurant;
use App\Food;

class Favorite extends Model
{
    use SoftDeletes;
    
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['ids'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
    public function get_data(){
        if($this->type == 'restaurant'){
            return Restaurant::where('id',$this->foreign_id)->first();
        }
        if($this->type == 'food'){
            return Food::where('id',$this->foreign_id)->first();
        }
    }
    public function getIdsAttribute(){
        if($this->type == 'restaurant'){
            return Restaurant::where('id',$this->foreign_id)->with('categories')->first();
        }
        if($this->type == 'food'){
            return Food::where('id',$this->foreign_id)->with('restaurant.categories')->first();
        }
    }
}
