<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Category extends Model
{
    use SoftDeletes;
    use HasSlug;

    protected $guarded = ['image'];
    
    public function getRouteKeyName()
    {
        return 'alias';
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image'];


    /**
     * The categories that belong to the restaurant.
     * @return BelongsToMany
     */
    public function restaurants()
    {
        return $this->belongsToMany('App\Restaurant','categories_restaurant');
    }

    /**
     * Get an image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return env('APP_URL').'/uploads/categories/'.$this->id.'.png';
    }

    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name_uz')
            ->saveSlugsTo('alias');
    }    
}
