<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

use Carbon\Carbon;
class Restaurant extends Model
{
    protected $connection='mysql';
    use SoftDeletes;
    use HasSlug;

    public function getRouteKeyName()
    {
        return 'alias';
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'payments' => 'array',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','desc_uz','desc_ru','phone','phone1','percentage','delivery_delta','ot_delta','phone2','lat','long','address','alias','district_id','address','shipping_free','payments','in_main'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image','cover','open'];

    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('alias');
    }

    /**
     * The categories that belong to the restaurant.
     *
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('App\Category','categories_restaurant');
    }

    /**
     * Get the foods for the restaurant.
     *
     * @return HasMany
     *
     *
     */
    public function foods()
    {
        return $this->hasMany('App\Food');
    }

    /**
     * Get the orders for the restaurant.
     *
     * @return HasMany
     */
    public function orders()
    {
        return $this->hasMany('App\Order')->withTrashed();
    }

    /**
     * Get the promos for the restaurant.
     *
     * @return HasMany
     */
    public function promos()
    {
        return $this->hasMany('App\Promo');
    }

    /**
     * The district that restaurant belongs to.
     *
     * @return BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('App\District');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }

    /**
     * Get the schedule for the restaurant.
     *
     * @return HasMany
     */
    public function schedules()
    {
        return $this->hasMany('App\Schedule');
    }
    /**
     * Get main restaurants.
     *
     */
    public function getMain()
    {
        return $this->where('in_main', '1')->get();
    }
    public function getId()
    {
       return dd($this->id);
    }
    public function getFoodId()
    {
        $ids = [0];
        foreach($this->foods as $k => $item){
            $ids[$k] = $item->id;
        }
       return $ids;
    }

    /**
     * Get an image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return env('APP_URL').'/uploads/restaurant/'.$this->id.'.jpg';
    }

    /**
     * Get an image.
     *
     * @return string
     */
    public function getCoverAttribute(){
        return env('APP_URL').'/uploads/restcover/'.$this->id.'.jpg';
    }

//    public function getDistanceAttribute($origin , $destination){
//        $response = \GoogleMaps::load('directions')
//        ->setParam([
//            'origin'          => $origin,
//            'destination'     => $destination,
//            'travel_mode'     => 'DRIVING',
//            "language"           => "ru-RU",
//        ])
//       ->get();
//        $data =  json_decode($response)->routes[0]->legs[0];
//        $distance = $data->distance->text;
//        $time = $data->duration->text;
//            $distance_km = 1 * filter_var($distance, FILTER_SANITIZE_NUMBER_INT);
//            if($distance_km < 40){
//            $price =  number_format(5000,0,'.',' ');
//        }
//        else{
//            $price =  number_format(round(( $distance_km - 40) * 150 + 5000, -2),0,'.',' ');
//        }
//        return response()->json(['distance' => $distance,'price' => $price, 'time' => $time]);
//    }
    public function getDistance($origin , $destination)
    {
        $response = \GoogleMaps::load('directions')
        ->setParam([
            'origin'          => $origin,
            'destination'     => $destination,
            'travel_mode'     => 'DRIVING',
            "language"           => "ru-RU",
        ])
       ->get();
        $data =  json_decode($response)->routes[0]->legs[0];
        $distance = $data->distance->text;
        $time = $data->duration->text;
            $distance_km = 1 * filter_var($distance, FILTER_SANITIZE_NUMBER_INT);
            if($distance_km < 40){
            $price =  number_format(5000,0,'.',' ');
        }
        else{
            $price =  number_format(round(( $distance_km - 40) * 150 + 5000, -2),0,'.',' ');
        }
        $data = ['distance' => $distance,'price' => $price, 'time' => $time];
        return $this->attributes['distance'] = $data;
//            return $this->attributes['distance'] = $this->id;
    }
    public function getOpenAttribute()
    {
        $today = Carbon::now()->dayOfWeek;
        if($today>0){
            $schedule = $this->schedules[$today - 1];
        }else{
            $schedule = $this->schedules[$today];
        }

        $open = Carbon::parse($schedule->open_hour)->format('H:i:s');
        $close   = Carbon::parse($schedule->close_hour)->format('H:i:s');
        $time  = Carbon::now()->format('H:i:s');

        if($time >= $open || $time <= $close) {
            return true;
        }
        return false;
    }
}
