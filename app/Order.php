<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasOne;
use NotificationChannels\WebPush\HasPushSubscriptions;
use App\Exceptions\PaymentException;

class Order extends Model
{
    public $timestamps = true;
    use SoftDeletes;
//    use HasPushSubscriptions;

    protected $guarded = ['price','food_id'];


    /**
     * Get order's review
     *
     * @return HasOne
     */
    public function review()
    {
        return $this->hasOne('App\Review');
    }
    /**
     * Get order's review
     *
     * @return HasOne
     */
    public function carrier_order()
    {
        return $this->hasOne('App\CarrierOrder','order_id');
    }
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }
    public function restaurant_trashed()
    {
        return $this->belongsTo('App\Restaurant')->withTrashed();
    }
    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }
    public function order_food()
    {
        return $this->hasMany('App\OrderFood');
    }
    public function order_food_trashed()
    {
        return $this->hasMany('App\OrderFood')->withTrashed();
    }
}
