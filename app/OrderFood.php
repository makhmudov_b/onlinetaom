<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderFood extends Model
{
    use SoftDeletes;

    protected $guarded =[];

    protected $appends = ['ids','size'];

    public function order()
    {
        return $this->belongsTo('App\Order')->withTrashed();
    }
    public function food()
    {
        return $this->belongsTo('App\Food','food_id')->withTrashed();
    }
    public function size()
    {
        return $this->hasOne('App\Size','id');
    }
    public function params()
    {
        return $this->hasMany('App\Param');
    }
    public function getIdsAttribute(){
        return Param::whereIn('id', json_decode($this->params))->get();
    }
    public function getSizeAttribute(){
        return Size::where('id', $this->size_id)->get();
    }
}
