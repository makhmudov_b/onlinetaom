<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;


class Food extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['image'];

    /**
     * Get Params for restaurant
     *
     * @return HasMany
     */
    public function params()
    {
        return $this->hasMany('App\Param');
    }
    /**
     * Get Size for restaurant
     *
     * @return HasMany
     */
    public function size()
    {
        return $this->hasMany('App\Size');
    }
    /**
     * Get Restaurant that Food belongs to
     *
     * @return BelongsTo
     */
    public function restaurant()
    {
        return $this->belongsTo('App\Restaurant');
    }

    /**
     * Get Orders that Food belongs to
     *
     * @return BelongsToMany
     */
    public function order_food()
    {
        return $this->hasOne('App\OrderFood');
    }
    /**
     * Get an image.
     *
     * @return string
     */
    public function getImageAttribute(){
        return env('APP_URL').'/uploads/food/'.$this->id.'.jpg';
    }
}
