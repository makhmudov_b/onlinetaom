<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestaurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('desc_uz',500);
            $table->string('desc_ru',500);
            $table->string('phone',9);
            $table->string('phone1',9)->nullable();
            $table->string('phone2',9)->nullable();
            $table->string('lat',50);
            $table->string('long',50);
            $table->unsignedInteger('delivery_delta')->default(0);
            $table->unsignedInteger('ot_delta')->default(2000);
            $table->unsignedInteger('district_id')->index();
            $table->string('payments');
            $table->integer('percentage')->default(10);
            $table->boolean('shipping_free')->index();
            $table->boolean('is_open')->default(1)->index();
            $table->boolean('in_main')->default(0)->index();
            $table->string('address',50);
            $table->string('alias',50)->unique()->index();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurants');
    }
}
