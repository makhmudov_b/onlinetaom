<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoriesRestaurant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_restaurant', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('restaurant_id')->unsigned();
            $table->integer('category_id')->unsigned();            
            $table->timestamps();
            $table->softDeletes();                 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_restaurant');
    }
}
