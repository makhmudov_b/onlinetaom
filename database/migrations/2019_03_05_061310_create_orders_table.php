<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('restaurant_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('long');
            $table->string('lat');
            $table->string('adress');
            $table->string('distance');
            $table->string('device_info');
            $table->integer('is_paid');
            $table->unsignedInteger('delivery_delta');
            $table->unsignedInteger('ot_delta');
            $table->string('payment_method');
            $table->string('status');
            $table->string('comment')->nullable();
            $table->string('shipping_time');
            $table->integer('cooking_time')->nullable();
            $table->integer('shipping_price');
            $table->integer('total_price');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
