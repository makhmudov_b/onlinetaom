<div class="modal deskt">
        <div class="modal-bg"></div>
        <div class="description-choice--desktop">
        <div class="description">
            <div class="pl-20">
                <button class="inner__arrow">
                    <img src="{{ asset('img/x.svg') }}">
                </button>
                <button class="inner__fav">
                    <img src="{{ asset('img/favorite.svg') }}">                    
                </button>
            </div>
        </div>
        <div class="description-info bg-white pb-10">
            <div class="pl-20 relative">
                    <div class="description-info__title">Острая Мясная</div>
                    <div class="description-info__name">Pizza Village</div>
                    <div class="description-info__desc">
                        Сыр моцарелла, ветчина, пепперони, халапеньо
                    </div>
                    <div class="description-info__price">57 000 сум</div>
                    <div class="description-info__add">
                        <span class="input-number-decrement">–</span><input class="input-number input-number-1" type="text" value="1" min="0" max="10"><span class="input-number-increment">+</span>
                    </div>
                </div>
            </div>                    
            <div class="pl-20">
                <div class="description-choice__title">
                    Выберите тесто
                </div>
                <div class="description-choice__type fsize">
                    <input type="radio" name="size" id="size_1" value="small" checked />
                    <label for="size_1">Традиционное</label>

                    <input type="radio" name="size" id="size_2" value="small" />
                    <label for="size_2">Пан-тесто</label>

                    <input type="radio" name="size" id="size_3" value="small" />
                    <label for="size_3">Тонкое</label>

                    <input type="radio" name="size" id="size_4" value="small" />
                    <label for="size_4">Воздушное</label>

                    <input type="radio" name="size" id="size_5" value="small" />
                    <label for="size_5">Сырный борт</label>

                    <input type="radio" name="size" id="size_6" value="small" />
                    <label for="size_6">Толстое</label>                    
                </div>                     
                <div class="description-choice__line"></div>
                <div class="description-choice__title">
                    Выберите размер
                </div>
            </div>
            <div class="description-choice__size">
                <input type="radio" name="choice" id="choice_1" value="small" checked="checked" />
                <label for="choice_1"><span class="size">19 см</span><span class="price">57 000 сум</span></label>
                <input type="radio" name="choice" id="choice_2" value="small" />
                <label for="choice_2"><span class="size">19 см</span><span class="price">57 000 сум</span></label>
                <input type="radio" name="choice" id="choice_3" value="small" />
                <label for="choice_3"><span class="size">19 см</span><span class="price">57 000 сум</span></label>                                
            </div>
            <div class="pl-20">
                <div class="description-choice__line"></div>
                <div class="description-choice__title">Добавки</div>
                <div class="description-choice__portion">
                    <div class="checkbox">  
                        <input id="check1" type="checkbox" name="check" value="check1">  
                        <label for="check1">
                            <img src="{{ asset('img/addition.png') }}">                                                    
                            <div class="checkbox__title">
                                Двойная начинка
                            </div>
                            <div class="checkbox__price">
                                7 500 сум
                            </div>
                        </label>
                        <input id="check2" type="checkbox" name="check" value="check2">  
                        <label for="check2">
                            <img src="{{ asset('img/addition.png') }}">                                                                                
                            <div class="checkbox__title">
                                Двойная начинка
                            </div>
                            <div class="checkbox__price">
                                7 500 сум
                            </div>
                        </label>  

                        <input id="check3" type="checkbox" name="check" value="check3">  
                        <label for="check3">
                            <img src="{{ asset('img/addition.png') }}">                                                                                
                            <div class="checkbox__title">
                                Двойная начинка
                            </div>
                            <div class="checkbox__price free">
                                бесплатно
                            </div>
                        </label>                          
                    </div>                    
                </div>              
            </div> 
            <div class="description-choice__bottom">
               <button><img src="{{ asset('img/plus.svg') }}"></button> Положить в корзину    
            </div>          
        </div>
<div class="popup-over" id="popup">
    <div class="profile">
        <div class="pl-20">
            <div class="text-left">
                Профиль
            </div>
            <img src="{{ asset('img/profile.png') }}" class="profile-photo">                
        </div>
    </div>
    <div class="profile-padding">
        <div class="profile__name">Khalil Mukhammad-Rakan</div>
        <div class="profile__phone">+998 94 619-67-67</div>
        <div class="profile__btn">
            Мои заказы
        </div>
        <div class="profile__btn">
            Редактировать данные
        </div>
        <div class="profile__btn">
            Выйти
        </div>                        
    </div>
</div>
</div>