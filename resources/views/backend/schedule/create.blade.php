@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Promo</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('PromoController@store',$id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Title_uz</label>
                            <input required="required" name="title_uz" type="text" class="form-control" id="food-name-uz" placeholder="Enter Title Uz">
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Title_ru</label>
                            <input required="required" name="title_ru" type="text" class="form-control" id="food-name-ru" placeholder="Enter Title Uz">
                        </div>
                        <div class="form-group">
                            <label for="startTime">StartTime</label>
                            <input required="required" name="start_time" type="date" id="startTime" min="2018-01-01">
                        </div>
                        <div class="form-group">
                            <label for="endTime">EndTime</label>
                            <input required="required" name="end_time" type="date" id="endTime" min="2018-01-01">
                        </div>                        
                        
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
