@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Districts</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('DistrictController@update',$id) }}" method="POST">
                        @method('PUT')                        
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Name Uz</label>
                            <input value="{{ $data->name_uz }}" name="name_uz" type="text" class="form-control" id="food-name-uz" placeholder="Enter Food Name">
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Name Ru</label>
                            <input value="{{ $data->name_ru }}" name="name_ru" type="text" class="form-control" id="food-name-ru" placeholder="Enter Food Name">
                        </div> 
                        <button class="btn btn-success">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
