@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Район</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('DistrictController@store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название Uz</label>
                            <input required="required" name="name_uz" type="text" class="form-control" id="food-name-uz" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Название Ru</label>
                            <input required="required" name="name_ru" type="text" class="form-control" id="food-name-ru" placeholder="Введите название">
                        </div>            
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
