@extends('layouts.app')

@section('content')
<div class="container-fluid pt-4">
@if(empty($user))
<a href="{{ action('OrderController@index') }}" class="btn btn-primary">Все</a>
<a href="{{ action('OrderController@getOrderBy','month') }}" class="btn btn-primary">За месяц</a>
<a href="{{ action('OrderController@getOrderBy','week') }}" class="btn btn-primary">За неделю</a>
<a href="{{ action('OrderController@getOrderBy','day') }}" class="btn btn-primary">За сегодня</a>
<form action="{{ action('OrderController@getBetween')  }}" method="POST" class="mt-2">
    @method('PATCH')
    @csrf
    От <input type="date" name="from" @if (!empty($from))value="{{ $from }}" @endif required>
    До <input type="date" name="to" @if (!empty($to))value="{{ $to}}" @endif required>
    <button type="submit" class="btn btn-primary">За Период</button>
</form>
@endif
    <div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">
        @if( empty($id) )
            Все
        @endif
        @if(!empty($user))
            заказы :{{$user}}
        @else
        Заказы
        @endif
        @if( !empty($id) )
            за
            {{ $id == 'month' ? 'Месяц' : ''  }}
            {{ $id == 'week' ? 'Неделю' : ''  }}
            {{ $id == 'day' ? 'День' : ''  }}
        @endif
    </h3>
        @if( empty($id) )
        <div>
        <a href="{{ action('OrderController@import') }}" class="btn btn-info">Excel</a>
        <a href="{{ action('ReviewController@show') }}" class="btn btn-primary">Все Отзывы</a>
        </div>
        @endif
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th class="w-1">No</th>
            <th>Дата</th>
            <th class="w-1">Пользователь</th>
            <th>Ресторан</th>
            <th>Курьер</th>
            <th>Статус</th>
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('callcenter'))
                <th>Ф\О</th>
                <th>Тип заказа</th>
                <th>Ответ</th>
                <th>Сумма</th>
                <th>Доставка</th>
                <th>Общая сумма</th>
            @endif
            @if(Auth::user()->hasRole('manager'))
                <th>Ответ</th>
                <th>Сумма</th>
            @endif
            @if(Auth::user()->hasRole('manager') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('callcenter'))
                <th></th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
            @if( !($datas->is_paid != 2 && $datas->payment_method == 1))
            @if($datas->status != 0)
        <tr
        @if($datas->status == 1) class="table-warning" @endif
        @if($datas->status == 2) class="table-info" @endif
        @if($datas->status == 3) class="table-primary" @endif
        @if($datas->status == 4) class="table-success" @endif
        >
            <td>#{{ $datas->id }}</td>
            <td>{{ \Carbon\Carbon::parse($datas->updated_at)->isoFormat('D MMMM HH:mm') }}</td>
            <td>{{ $datas->user->phone }}</td>
            @if($datas->restaurant !== null)
            <td>{{ $datas->restaurant->name }}</td>
                @if($datas->carrier_order)
                    @if($datas->carrier_order->status == '0')
                        <td class="bg-danger tag-danger">
                        Не принял
                        </td>
                    @else
                        <td>
                        {{ $datas->carrier_order->user ? $datas->carrier_order->user->name : null }}
                        </td>
                    @endif
                    @else
                    <td>
                        Не принял
                    </td>
                @endif
            @else
            <td>Не выбран</td>
            <td>Не оформлен</td>
            @endif
{{--            @if($datas->user->phone !== null)--}}
{{--                <td>{{ $datas->user->phone }}</td>--}}
{{--            @else--}}
{{--                <td>Нет Номера</td>--}}
{{--            @endif--}}
            @if($datas->status == -1)
                <td>Отменен</td>
            @endif
            @if($datas->status == 0)
                <td>В корзине</td>
            @endif
            @if($datas->status == 1)
                <td>Оформлен</td>
            @endif
            @if($datas->status == 2)
                <td>Принят</td>
            @endif
            @if($datas->status == 3)
                <td>В пути</td>
            @endif
            @if($datas->status == 4)
                <td>Доставлен</td>
            @endif
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('callcenter'))
                @if($datas->status != 0)
                    @if($datas->payment_method == 0)
                        <td>Наличные <img src="{{ asset('backend/images/flags/uz.svg')  }}" width="20" class="rounded" /></td>
                    @else
                        <td><img src="{{ asset('backend/images/flags/payme.svg')  }}" width="50" class="rounded" /></td>
                    @endif
                @else
                <td>Не выбрано</td>
                @endif
                <td>{{ $datas->shipping_time !== '0' ? $datas->shipping_time : 'Не указано'  }}</td>
            @endif
            <td @if($datas->status == 1) class="time" data-updated="{{ \Carbon\Carbon::parse($datas->updated_at)->tz('UTC')->Format('D M d Y H:i:s e O T') }}" @endif>
                @if($datas->status == 0)
                    Не оформлен
                @endif
                @if($datas->status == 1)
                      <div class="loader"></div>
                @endif
                @if($datas->status == 2)
                      Отвечен
                @endif
                @if($datas->status == 3)
                      Отвечен
                @endif
                @if($datas->status == 4)
                      Отвечен
                @endif
            </td>
            <td>{{ number_format($datas->total_price - $datas->shipping_price) }}</td>
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('callcenter'))
            <td>{{ number_format($datas->shipping_price) }}</td>
            <td>{{ number_format($datas->total_price) }}</td>
            @endif
            @if(Auth::user()->hasRole('manager') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('callcenter'))
            <td class="text-center">
            @if($datas->restaurant !== null && $datas->status != 0)
            @if($datas->carrier_order != null)
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('callcenter'))
                <a href="{{ action('ReviewController@index', $datas->id)  }}" class="btn btn-secondary btn-sm">
                    Отзыв
                </a>
                @if($datas->carrier_order->status == '1')
                    <a href="{{ action('CarrierController@editOrder', $datas->carrier_order->id)  }}" class="btn btn-secondary btn-sm">
                        Изменить курьера
                    </a>
                @endif
            @endif
            @endif
            <a href="{{ action('OrderController@showInner', $datas->id)  }}" class="btn btn-secondary btn-sm">
                <i class="fe fe-arrow-right"></i>
            </a>
            @endif
            </td>
            @endif
        </tr>
        @endif
        @endif
        @endforeach
        </tbody>
    </table>
    </div>
</div>
    @if(empty($from))
        {{ $data->links() }}
    @endif
</div>
</div>
@endsection
@section('script')
<script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js') }}"></script>
<script>
$(document).ready(function(){
    var get = $('.time');
get.each(function(el) {
    var data = $(this).data('updated')
    var parent = $(this)
    function changeHtml(){
        parent.html(makeTimer(data))
        if(parent.find('span.bg-danger').length > 0){
            parent.parent().addClass('table-danger')
        }
        else{
            parent.parent().addClass('table-success')
        }
    }
    setInterval(changeHtml, 1000)
});
  setTimeout(function(){
    location = ''
  }, 60000)
})

function makeTimer(data) {
        var startTime = new Date(data);
			startTime = (Date.parse(startTime) / 1000);
        //
			var now = new Date();
			now = (Date.parse(now) / 1000);
        //
			var timeLeft =  now - startTime ;
        //
			var days = Math.floor(timeLeft / 86400);
			var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
			var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
			var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

			// if (hours < "10") { hours = "0" + hours; }
			if (minutes < "10") { minutes = "0" + minutes; }
			// if (seconds < "10") { seconds = "0" + seconds; }
            if(minutes > 5){
                return '<span class="status-icon bg-danger"></span> ' + minutes + " Минут "
            }
            return '<span class="status-icon bg-success"></span> ' + minutes + " Минут "
	}

</script>
@endsection
