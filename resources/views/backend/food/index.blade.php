@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        <h3 class="card-title">Блюда</h3>
        @if(Auth::user()->hasRole('admin'))
         <a class="btn btn-sm btn-outline-success" href="{{ action('FoodController@create','restaurant_id='.$rest_id) }}">Добавить</a>
        @endif
        </div>
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
            <tr>
                <th>Название</th>
                <th>Фотография</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)                    
            <tr>
                <td>{{ str_limit($datas->name_ru ,20,'...') }}</td>
                <td>
                    <img src="{{ asset('uploads/food/'.$datas->id.'.jpg') }}" width="140">
                </td>
                @if(Auth::user()->hasRole('admin'))
                @if($datas->deleted_at === null)
                <td class="text-right">
                    <a href="{{ action('FoodController@params' , $datas->id) }}" class="btn btn-secondary btn-sm">Параметры</a>
                    <a href="{{ action('FoodController@size' , $datas->id) }}" class="btn btn-secondary btn-sm">Размер</a>
                </td>
                @else
                <td></td>
                @endif
                @endif

                <td class="text-center">
                    @if($datas->deleted_at !== null)
                    <form class="d-inline-block" action="{{ action('FoodController@restore' , $datas->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-success">
                                <i class="fe fe-refresh-ccw"></i> Включить
                        </button>
                    </form>
                        @else
                    <form class="d-inline-block" action="{{ action('FoodController@delete' , $datas->id) }}" onclick="return confirm('Вы уверены?')" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger">
                            <i class="fe fe-x"></i> Отключить
                    </button>
                    </form>
                    @if(Auth::user()->hasRole('admin'))
                        <a href="{{ action('FoodController@edit' , $datas->id) }}" class="btn icon border-0"><i class="fe fe-edit"></i></a>
                    @endif
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
