@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Курьер</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CarrierController@update', $data->id) }}" method="POST">
                        @method('PUT')
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Имя</label>
                            <input name="name" value="{{ $data->name  }}" type="text" class="form-control" id="food-name-uz" placeholder="Введите Имя" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Номер телефона</label>
                            <input name="phone_number" value="{{ $data->phone_number  }}" type="number" class="form-control" id="food-desc-uz" placeholder="Введите телефон" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="phone">Номер машины</label>
                            <input name="car_number" value="{{ $data->car_number  }}" type="text" class="form-control" id="phone" placeholder="Введите Номер машины" checked="checked" />
                        </div>
                        <div class="form-group">
                            <label for="food-desc-uz">Новый пароль</label>
                            <input type="text" class="form-control" name="password" />
                        </div>
                        <button class="btn btn-success">Изменить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
