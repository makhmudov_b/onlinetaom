@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
        <h3 class="card-title">Заказы Курьера @if(empty($data))<span class="badge-primary p-1 rounded">{{ $data[0]->user->name }}</span>@endif</h3>
        </div>
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
            <tr>
                <th>No</th>
                <th>Дата</th>
                <th>Ресторан</th>
                <th>Адрес Доставки</th>
                <th>Дистанция</th>
                <th>Статус</th>
                <th>Доставка</th>
                <th>Общая сумма</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)
            <tr>
                <td>#{{ $datas->id }}</td>
                <td>{{ $datas->updated_at->isoFormat('D MMMM HH:mm') }}</td>
                <td>@if($datas->restaurant){{ $datas->restaurant->name }}@endif</td>
                <td>{{ $datas->street_name }}</td>
                <td>{{ $datas->distance }} км</td>
                <td>
                    @if($datas->status == '0') Не принят @endif
                    @if($datas->status == '1') Принят @endif
                    @if($datas->status == '2') Взял еду @endif
                    @if($datas->status == '3') Доставлен @endif
                </td>
                <td>{{ $datas->delivery_price }}</td>
                <td>{{ $datas->full_price }}</td>
{{--                <td class="text-right">--}}
{{--                    <a href="{{ action('CarrierController@editOrder' , $datas->id) }}" class="btn icon border-0"><i class="fe fe-edit"></i></a>--}}
{{--                </td>--}}
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>
@endsection
