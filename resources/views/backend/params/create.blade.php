@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Параметр</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('ParamController@store',$id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название Uz</label>
                            <input required="required" name="name" type="text" class="form-control" id="food-name-uz" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="food-price">Цена UZS</label>
                            <input required="required" name="price" type="number" class="form-control" id="food-price" placeholder="Введите цену" min="0">
                        </div>
                        <div class="form-group">
                            Изображение <span class="font-weight-bold">200x200</span>
                            <input required="required" type="file" class="form-control" name="image">
                        </div>                         
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
