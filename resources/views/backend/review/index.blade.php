@extends('layouts.app')

@section('content')
<div class="container">
<div class="card my-3 my-md-5">
    <div class="card-header justify-content-between">
    <h3 class="card-title">Отзыв</h3>
    </div>
    <div class="table-responsive">
    <table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            @if(count($data) > 1)
            <th>Заказ №</th>
            @endif
            <th>Оценка</th>
            <th>Отзыв</th>
        </tr>
        </thead>
        <tbody>
                @foreach($data as $datas)
                    <tr>
                        @if(count($data) > 1)
                            <td>{{ $datas->order->id }}</td>
                        @endif
                        <td>{{ $datas->mark }} Баллов</td>
                        <td>{{ $datas->message }}</td>
                    </tr>
                @endforeach

        </tbody>
    </table>
    </div>
</div>
</div>
@endsection
