@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card my-3 my-md-5">
        <div class="card-header justify-content-between">
            <h3 class="card-title">Избранное</h3>
        </div>
        <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap">
            <thead>
            <tr>
                <th>Ползователь</th>
                <th>Ресторан </th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $datas)
            <tr>
                <td>{{ $datas->user->name }}</td>
                @if($datas->ids !== null)
                    @if($datas->type == 'restaurant')
                        <td>{{ $datas->ids->name }}</td>
                    @endif
                    @if($datas->type == 'food')
                        <td>{{ $datas->ids->name_ru }}</td>
                    @endif
                @else
                    <td>Удалено</td>
                @endif
            </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>        
</div>
@endsection
