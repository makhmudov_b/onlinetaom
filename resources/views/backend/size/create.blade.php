@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Добавить Размер</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('SizeController@store',$id) }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название</label>
                        <input required="required" name="name" type="text" class="form-control" id="food-name-uz" placeholder="Введите Название">
                        </div>
                        <div class="form-group">
                            <label for="food-price">Цена (сум)</label>
                            <input required="required" name="price" type="number" class="form-control" id="food-price" placeholder="Введите Цену" min="0">
                        </div>
                        <button class="btn btn-success">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
