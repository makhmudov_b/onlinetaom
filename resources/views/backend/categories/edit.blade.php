@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Изменить</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ action('CategoryController@update',$id) }}" method="POST">
                        @method('PUT')                        
                        @csrf
                        <div class="form-group">
                            <label for="food-name-uz">Название Uz</label>
                            <input value="{{ $data->name_uz }}" name="name_uz" type="text" class="form-control" id="food-name-uz" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <label for="food-name-ru">Название Ru</label>
                            <input value="{{ $data->name_ru }}" name="name_ru" type="text" class="form-control" id="food-name-ru" placeholder="Введите название">
                        </div>
                        <div class="form-group">
                            <img src="{{ asset('uploads/categories/'.$data->id.'.png') }}" width="140">
                            Для изменения загрузите новое фото <span class="font-weight-bold">200x200</span>
                            <input type="file" class="form-control" name="image">
                        </div>
                        <button class="btn btn-success">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
