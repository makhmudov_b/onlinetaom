<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet" />

    <!-- GCM Manifest (optional if VAPID is used) -->
    @if (config('webpush.gcm.sender_id'))
        <link rel="manifest" href="/manifest.json">
    @endif

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'user' => Auth::user(),
            'csrfToken' => csrf_token(),
            'vapidPublicKey' => config('webpush.vapid.public_key'),
            'pusher' => [
                'key' => config('broadcasting.connections.pusher.key'),
                'cluster' => config('broadcasting.connections.pusher.options.cluster'),
            ]
        ]) !!};
    </script>
</head>
<body>
    <div id="app" class="page" v-cloak>
        <div class="page-main">
    <div class="header py-4">
        <div class="container">
        <div class="d-flex">
            <a class="header-brand" href="/home">
            <img src="{{ asset('img/logo.svg') }}"  width="70">
            </a>
            <div class="d-flex order-lg-2 ml-auto">
            <div class="dropdown">
                <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                <span class="avatar" style="    "></span>
                <span class="ml-2 d-none d-lg-block">
                    <span class="text-default">{{ Auth::user()->name }}</span>
                </span>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                <a class="dropdown-item" href="/notify">
                    <i class="dropdown-icon fe fe-settings"></i> Уведомление
                </a>
                <div class="dropdown-divider"></div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                        <button class="dropdown-item" type="submit"><i class="dropdown-icon fe fe-log-out"></i> Выйти</button>
                </form>
                </div>
            </div>
            </div>
            <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse" data-target="#headerMenuCollapse">
            <span class="header-toggler-icon"></span>
            </a>
        </div>
        </div>
    </div>


        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Dashboard</div>

                        <div class="panel-body text-center">
                            {{-- See resources/assets/js/components/NotificationsDemo.vue --}}
                            <notifications-demo></notifications-demo>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script src="{{asset('js/vue.js')}}"></script>
</body>
</html>
