<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="profile mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Профиль
                </div>
                <img src="{{ asset('img/profile.png') }}" class="profile-photo">                
            </div>
        </div>
        <div class="profile-padding">
            <div class="profile__name">Khalil Mukhammad-Rakan</div>
            <div class="profile__phone">+998 94 619-67-67</div>
            <div class="profile__btn">
                Мои заказы
            </div>
            <div class="profile__btn">
                Редактировать данные
            </div>
            <div class="profile__btn">
                Выйти
            </div>                        
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('scripts')
    </body>
</html>
