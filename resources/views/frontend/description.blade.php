<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="description mobile">
            <div class="container">
                <button class="inner__arrow">
                    <img src="{{ asset('img/arrow.svg') }}">
                </button>
                <button class="inner__fav">
                    <img src="{{ asset('img/favorite.svg') }}">                    
                </button>
            </div>
        </div>
        <div class="description-info">
            <div class="container">
                <div class="description-info__title">Острая Мясная</div>
                <div class="description-info__name">Pizza Village</div>
                <div class="description-info__desc">
                    Сыр моцарелла, ветчина, пепперони, халапеньо
                </div>
                <div class="description-info__price">57 000 сум</div>
                <div class="description-info__add">
                    <span class="input-number-decrement">–</span><input class="input-number input-number-1" type="text" value="1" min="0" max="10"><span class="input-number-increment">+</span>
                </div>
            </div>
        </div>
        <div class="description-choice">
            <div class="container">
                <div class="description-choice__title">
                    Выберите тесто
                </div>
                <div class="description-choice__type">
                    <input type="radio" name="size" id="size_1" value="small" checked />
                    <label for="size_1">Традиционное</label>

                    <input type="radio" name="size" id="size_2" value="small" />
                    <label for="size_2">Пан-тесто</label>

                    <input type="radio" name="size" id="size_3" value="small" />
                    <label for="size_3">Тонкое</label>

                    <input type="radio" name="size" id="size_4" value="small" />
                    <label for="size_4">Воздушное</label>

                    <input type="radio" name="size" id="size_5" value="small" />
                    <label for="size_5">Сырный борт</label>

                    <input type="radio" name="size" id="size_6" value="small" />
                    <label for="size_6">Толстое</label>                    
                </div>                     
                <div class="description-choice__line"></div>
                <div class="description-choice__title">
                    Выберите размер
                </div>
            </div>
            <div class="description-choice__size">
                <input type="radio" name="choice" id="choice_1" value="small" checked="checked" />
                <label for="choice_1"><span class="size">19 см</span><span class="price">57 000 сум</span></label>
                <input type="radio" name="choice" id="choice_2" value="small" />
                <label for="choice_2"><span class="size">19 см</span><span class="price">57 000 сум</span></label>
                <input type="radio" name="choice" id="choice_3" value="small" />
                <label for="choice_3"><span class="size">19 см</span><span class="price">57 000 сум</span></label>                                
            </div>     
            <div class="container">
                <div class="description-choice__line"></div>
                <div class="description-choice__title">Добавки</div>
                <div class="description-choice__portion">
                    <div class="checkbox">  
                        <input id="check1" type="checkbox" name="check" value="check1">  
                        <label for="check1">
                            <img src="{{ asset('img/addition.png') }}">                                                    
                            <div class="checkbox__title">
                                Двойная начинка
                            </div>
                            <div class="checkbox__price">
                                7 500 сум
                            </div>
                        </label>
                        <input id="check2" type="checkbox" name="check" value="check2">  
                        <label for="check2">
                            <img src="{{ asset('img/addition.png') }}">                                                                                
                            <div class="checkbox__title">
                                Двойная начинка
                            </div>
                            <div class="checkbox__price">
                                7 500 сум
                            </div>
                        </label>  

                        <input id="check3" type="checkbox" name="check" value="check3">  
                        <label for="check3">
                            <img src="{{ asset('img/addition.png') }}">                                                                                
                            <div class="checkbox__title">
                                Двойная начинка
                            </div>
                            <div class="checkbox__price free">
                                бесплатно
                            </div>
                        </label>                          
                    </div>                    
                </div>              
            </div> 
            <div class="description-choice__bottom">
               <button><img src="{{ asset('img/plus.svg') }}"></button> Положить в корзину    
            </div>          
        </div>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script>
(function() {
 
  window.inputNumber = function(el) {

    var min = el.attr('min') || false;
    var max = el.attr('max') || false;

    var els = {};

    els.dec = el.prev();
    els.inc = el.next();

    el.each(function() {
      init($(this));
    });

    function init(el) {

      els.dec.on('click', decrement);
      els.inc.on('click', increment);

      function decrement() {
        var value = el[0].value;
        value--;
        if(!min || value >= min) {
          el[0].value = value;
        }
      }

      function increment() {
        var value = el[0].value;
        value++;
        if(!max || value <= max) {
          el[0].value = value++;
        }
      }
    }
  }
})();

inputNumber($('.input-number-1'));             
        </script>
        @yield('scripts')
    </body>
</html>
