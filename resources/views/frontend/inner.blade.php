<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="wrapper">
        @include('partials.navdesktop')
        <section class="favorite-rest desktop">
            <div class="container cont-fix flex pt-40 pb-40">
            <div class="flex__bottom">
                <img src="{{ asset('img/innbrand1.png') }}" width="80" class="vm">
                <div class="favorite-rest__text">
                    <span class="favorite-rest__text-title">Pizza Village</span>
                    <p class="favorite-rest__text-p">Пиццерия</p>
                </div>
                <div class="favorite-rest__btn">
                    <img src="{{ asset('img/favorite.svg') }}">                    
                </div>
            </div>
            <div class="flex__bottom">
                <div class="favorite-rest__info">
                    Небольшое краткое описание ресторана, состоящее из одного или нескольких очень коротких, но привлекательных предложений.
                </div>
            </div>
            <div class="flex__bottom flex-auto">
                <div class="favorite-rest__el">
                    <div class="favorite-rest__el-whole">
                        <div class="favorite-rest__el--btn active">
                            <img src="{{ asset('img/truck.svg') }}">
                        </div>
                        <div class="favorite-rest__el--info active-bold">
                            14 400 сум
                        </div>
                    </div>
                    <div class="favorite-rest__el-whole">
                        <div class="favorite-rest__el--btn">
                            <img src="{{ asset('img/longitude.svg') }}">
                        </div>
                        <div class="favorite-rest__el--info">
                            2.6 км
                        </div>
                    </div>
                    <div class="favorite-rest__el-whole">
                        <div class="favorite-rest__el--btn">
                            <img src="{{ asset('img/time.svg') }}">
                        </div>
                        <div class="favorite-rest__el--info">
                            8:00 - 23:00
                        </div>
                    </div>                                        
                </div>
            </div>
            </div>
        </section>
        <div class="favorite desktop">
            <button class="favorite__btn-1 active">Пицца</button>
            <button class="favorite__btn-1">Закуски</button>
            <button class="favorite__btn-1">Салаты</button>
            <button class="favorite__btn-1">Напитки</button>
            <button class="favorite__btn-1">Десерты</button>
            <button class="favorite__btn-1">Акции</button>
        </div>        
        <section class="food desktop">
            <div class="container cont-fix">
                <div class="flex">
                <div class="food__el">
                    <img src="{{ asset('img/food.png') }}" class="food__el-img">
                    <button class="food__el-add">
                        <img src="{{ asset('img/add.svg') }}">
                    </button>                    
                    <div class="food__el-main">
                        <span class="food__el-main--title">
                            Куриная Барбекю
                        </span>
                        <span class="food__el-main--price">
                            42 500 сум
                        </span>
                        <p class="food__el-main--info">
                            Соус Барбекю, бекон, курица, красный лук, сладкий перец, грибы, сыр моцарелла, жареный лук
                        </p>
                    </div>
                </div>
                <div class="food__el">
                    <img src="{{ asset('img/food.png') }}" class="food__el-img">
                    <button class="food__el-add">
                        <img src="{{ asset('img/add.svg') }}">
                    </button>                    
                    <div class="food__el-main">
                        <span class="food__el-main--title">
                            Куриная Барбекю
                        </span>
                        <span class="food__el-main--price">
                            42 500 сум
                        </span>
                        <p class="food__el-main--info">
                            Соус Барбекю, бекон, курица, красный лук, сладкий перец, грибы, сыр моцарелла, жареный лук
                        </p>
                    </div>
                </div>   
                <div class="food__el">
                    <img src="{{ asset('img/food.png') }}" class="food__el-img">
                    <button class="food__el-add">
                        <img src="{{ asset('img/add.svg') }}">
                    </button>                    
                    <div class="food__el-main">
                        <span class="food__el-main--title">
                            Куриная Барбекю
                        </span>
                        <span class="food__el-main--price">
                            42 500 сум
                        </span>
                        <p class="food__el-main--info">
                            Соус Барбекю, бекон, курица, красный лук, сладкий перец, грибы, сыр моцарелла, жареный лук
                        </p>
                    </div>
                </div>
                <div class="food__el">
                    <img src="{{ asset('img/food.png') }}" class="food__el-img">
                    <button class="food__el-add">
                        <img src="{{ asset('img/add.svg') }}">
                    </button>                    
                    <div class="food__el-main">
                        <span class="food__el-main--title">
                            Куриная Барбекю
                        </span>
                        <span class="food__el-main--price">
                            42 500 сум
                        </span>
                        <p class="food__el-main--info">
                            Соус Барбекю, бекон, курица, красный лук, сладкий перец, грибы, сыр моцарелла, жареный лук
                        </p>
                    </div>
                </div>
                <div class="food__el">
                    <img src="{{ asset('img/food.png') }}" class="food__el-img">
                    <button class="food__el-add">
                        <img src="{{ asset('img/add.svg') }}">
                    </button>                    
                    <div class="food__el-main">
                        <span class="food__el-main--title">
                            Куриная Барбекю
                        </span>
                        <span class="food__el-main--price">
                            42 500 сум
                        </span>
                        <p class="food__el-main--info">
                            Соус Барбекю, бекон, курица, красный лук, сладкий перец, грибы, сыр моцарелла, жареный лук
                        </p>
                    </div>
                </div>
                <div class="food__el">
                    <img src="{{ asset('img/food.png') }}" class="food__el-img">
                    <button class="food__el-add">
                        <img src="{{ asset('img/add.svg') }}">
                    </button>                    
                    <div class="food__el-main">
                        <span class="food__el-main--title">
                            Куриная Барбекю
                        </span>
                        <span class="food__el-main--price">
                            42 500 сум
                        </span>
                        <p class="food__el-main--info">
                            Соус Барбекю, бекон, курица, красный лук, сладкий перец, грибы, сыр моцарелла, жареный лук
                        </p>
                    </div>
                </div>                                                   
            </div>
        </div>
    </section>        
        <div class="inner mobile">
            <div class="container">
                <button class="inner__arrow">
                    <img src="{{ asset('img/arrow.svg') }}">
                </button>
                <button class="inner__fav">
                    <img src="{{ asset('img/favorite.svg') }}">                    
                </button>
                <img src="{{ asset('img/innbrand.png') }}" width="140" class="inner__brand">                                    
            </div>
        </div>
        <div class="inner-info mobile">
            <div class="container">
                <h4 class="inner-info__title">Pizza Village</h4>
                <p class="inner-info__desc">Пиццерия</p>  
                <div class="col-xs-6">
                    <ul class="inner-info__ul">
                        <li class="active"><button class="inner-info__btn"><img src="{{ asset('img/inner1.svg') }}"></button> 14 400 сум</li>    
                        <li><button class="inner-info__btn"><img src="{{ asset('img/inner2.svg') }}"></button> 2.6 км </li>    
                        <li><button class="inner-info__btn"><img src="{{ asset('img/inner3.svg') }}"></button> 8:00 - 23:00</li>    
                    </ul>    
                </div> 
                <div class="col-xs-6">
                    <div class="inner-info__p">
                            Рекламное сообщество, на первый взгляд, не критично. Отсюда естественно следует, что имидж.                        
                    </div>    
                </div> 
            </div>
        </div>
        <div class="rest-list mobile">
            <div class="rest-list__inner active">
                Пицца
            </div>
            <div class="rest-list__inner">
                Закуски
            </div>
            <div class="rest-list__inner">
                Салаты
            </div>
            <div class="rest-list__inner">
                Напитки
            </div>
        </div>
        <div class="inner-choice mobile">
            <div class="inner-choice__wrapper">
                <img src="{{ asset('img/choice.png') }}" class="inner-choice--img">
                <div class="inner-choice--text">
                    <div class="inner-choice__title">
                        Терияки
                    </div>
                    <div class="inner-choice__price">
                        42 500 сум
                    </div>
                </div>
                <button class="inner-choice__btn">
                    <img src="{{ asset('img/add.svg') }}">                                        
                </button>
            </div>
            <div class="inner-choice__wrapper">
                <img src="{{ asset('img/choice.png') }}" class="inner-choice--img">
                <div class="inner-choice--text">
                    <div class="inner-choice__title">
                        Терияки
                    </div>
                    <div class="inner-choice__price">
                        42 500 сум
                    </div>
                </div>
                <button class="inner-choice__btn">
                    <img src="{{ asset('img/add.svg') }}">                                        
                </button>
            </div>
            <div class="inner-choice__wrapper">
                <img src="{{ asset('img/choice.png') }}" class="inner-choice--img">
                <div class="inner-choice--text">
                    <div class="inner-choice__title">
                        Терияки
                    </div>
                    <div class="inner-choice__price">
                        42 500 сум
                    </div>
                </div>
                <button class="inner-choice__btn">
                    <img src="{{ asset('img/add.svg') }}">                                        
                </button>
            </div>                        
            <div class="inner-choice__wrapper">
                <img src="{{ asset('img/choice.png') }}" class="inner-choice--img">
                <div class="inner-choice--text">
                    <div class="inner-choice__title">
                        Терияки
                    </div>
                    <div class="inner-choice__price">
                        42 500 сум
                    </div>
                </div>
                <button class="inner-choice__btn">
                    <img src="{{ asset('img/add.svg') }}">                                        
                </button>
            </div>
            <div class="inner-choice__wrapper">
                <img src="{{ asset('img/choice.png') }}" class="inner-choice--img">
                <div class="inner-choice--text">
                    <div class="inner-choice__title">
                        Терияки
                    </div>
                    <div class="inner-choice__price">
                        42 500 сум
                    </div>
                </div>
                <button class="inner-choice__btn">
                    <img src="{{ asset('img/add.svg') }}">                                        
                </button>
            </div>                        
        </div>
        @include('partials.footer')
    </div>
    @include('partials.modal')    
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script>
            $('.rest-list').slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: false,
                dots:false,
            });
(function() {
 
  window.inputNumber = function(el) {

    var min = el.attr('min') || false;
    var max = el.attr('max') || false;

    var els = {};

    els.dec = el.prev();
    els.inc = el.next();

    el.each(function() {
      init($(this));
    });

    function init(el) {

      els.dec.on('click', decrement);
      els.inc.on('click', increment);

      function decrement() {
        var value = el[0].value;
        value--;
        if(!min || value >= min) {
          el[0].value = value;
        }
      }

      function increment() {
        var value = el[0].value;
        value++;
        if(!max || value <= max) {
          el[0].value = value++;
        }
      }
    }
  }
})();

inputNumber($('.input-number-1'));        

var modal = $('.modal');
var modalback = $('.modal-bg');

// modal.fadeOut('fast');
// modalback.fadeOut('fast');

$(document).ready(function(){
    var el = $('.food__el');

    el.on('click',function(){
        modal.fadeIn('fast');
        modalback.fadeIn('fast');
        $('body').css({overflow:'hidden'});
    });
    modalback.on('click',function(){
        modal.fadeOut('fast');
        modalback.fadeOut('fast');
        $('body').css({overflow:'auto'});                
    });
});     
        </script>
        @yield('scripts')
    </body>
</html>
