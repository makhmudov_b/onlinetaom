<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>OnlineTaom</title>
   <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
   <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>

<body>
   <div class="wrapper">
   @include('partials.nav')

<section class="basket desktop">
<div class="basket__title">
      <img src="{{ asset('img/cart-green.svg') }}">
      <h2>
      Моя корзина
      </h2>
</div>
<div class="container cont-fix rest">
      <div class="col-md-8 col-sm-8 col-xs-7 padding-fix">
      <img src="{{ asset('img/brand1.png') }}" class="rest--img">
      <div class="rest--info">
            <h3 class="rest__name">Caffee’issimo</h3>
            <p class="rest__type">Европейская кухня</p>
      </div>
      </div>
      <div class="rest__delivery text-center col-md-2 col-sm-2 col-xs-3 padding-fix">
      <div class="rest__delivery--wrap">
            <img src="{{ asset('img/truck.svg') }}">
      </div>
      <p>12000 сум</p>
      </div>
      <div class="rest__location text-center col-md-2 col-sm-2 col-xs-2 padding-fix">
      <img src="{{ asset('img/longitude.svg') }}">
      <p>4.2 км</p>
      </div>
</div>
</section>

<section class="basket-back desktop small-hidden">
   <div class="container cont-fix">
      <div class="item">
      <div class="item__text">
         <img src="{{ asset('img/basket.png') }}" width="100">
         <h3>Запеченый окорок</h3>
      </div>
      <div class="item__p">
         Рекламный бриф, вопреки мнению, основан на опыте применения.
      </div>
      <div class="item__num">
      <span class="input-number-decrement">–</span>
            <input class="input-number input-number-1" type="text" value="1" min="1" max="10">
      <span class="input-number-increment">+</span>
      </div>
      <div class="item__price-2">
      74 000 сум
      </div>
      <button class="delete">
      <img src="{{ asset('img/delete.svg') }}" width="30">
      </button>
      </div>
      <div class="item">
         <div class="item__text">
            <img src="{{ asset('img/basket.png') }}" width="100">
            <h3>Запеченый окорок</h3>
         </div>
         <div class="item__p">
            Рекламный бриф, вопреки мнению, основан на опыте применения.
         </div>
         <div class="item__num">
            <span class="input-number-decrement">–</span>
               <input class="input-number input-number-2" type="text" value="1"
               min="1" max="10">
            <span class="input-number-increment">+</span>
         </div>
         <div class="item__price-2">
            74 000 сум
         </div>
         <button class="delete">
            <img src="{{ asset('img/delete.svg') }}" width="30">
         </button>
      </div>
      <div class="item">
         <div class="item__text">
            <img src="{{ asset('img/basket.png') }}" width="100">
            <h3>Запеченый окорок</h3>
         </div>
         <div class="item__p">
            Рекламный бриф, вопреки мнению, основан на опыте применения.
         </div>
         <div class="item__num">
            <span class="input-number-decrement">–</span>
               <input class="input-number input-number-3" type="text" value="1"
               min="1" max="10">
            <span class="input-number-increment">+</span>
         </div>
         <div class="item__price-2">
            74 000 сум
         </div>
         <button class="delete">
            <img src="{{ asset('img/delete.svg') }}" width="30">
         </button>
      </div>
      <div class="item__checkout">
         <div class="col-md-6 col-sm-6 text-left">
            <button class="basket__btn">Оформить заказ</button>
         </div>         
         <div class="col-md-6 col-sm-6 text-right">
            <div class="basket__info">
               <span>Итого с доставкой:</span>
               <span class="price">174 000 сум</span>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
   </div>
</section>
<div class="events mobile">
      <div class="container">
            <button class="events__arrow">
            <img src="{{ asset('img/arrowgreen.svg') }}">
            </button>
            <div class="text-center">
            Моя корзина
            </div>
      </div>
</div>
<div class="basket mobile">
      <div class="restaurant__more">
            <img src="{{ asset('img/brand1.png') }}" width="60" class="vm"> Caffee’issimo
            <div class="restaurant__more-inner">
            <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div>
            <p class="restaurant__long pt-5">4.2 км</p>
            </div>
      </div>
</div>
<div class="restaurant small-show">
<div class="restaurant__ins-2">
      <div class="restaurant__text">
      <img src="{{ asset('img/basket.png') }}" width="50" class="vm"> Запеченый окорок
      <button class="delete">
      <img src="{{ asset('img/delete.svg') }}">
      </button>
      </div>
      <div class="restaurant__p">
      Рекламный бриф, вопреки мнению, основан на опыте применения.
      </div>
      <div class="restaurant__price-2">
      74 000 сум
      </div>
      <div class="restaurant__num">
      <span class="input-number-decrement">–</span><input class="input-number input-number-1" type="text" value="1"
            min="1" max="10"><span class="input-number-increment">+</span>
      </div>
</div>
<div class="restaurant__ins-2">
      <div class="restaurant__text">
      <img src="{{ asset('img/basket.png') }}" width="50" class="vm"> Запеченый окорок
      <button class="delete">
            <img src="{{ asset('img/delete.svg') }}">
      </button>
      </div>
      <div class="restaurant__p">
      Рекламный бриф, вопреки мнению, основан на опыте применения.
      </div>
      <div class="restaurant__price-2">
      74 000 сум
      </div>
      <div class="restaurant__num">
      <span class="input-number-decrement">–</span><input class="input-number input-number-4" type="text" value="1"
            min="1" max="10"><span class="input-number-increment">+</span>
      </div>
</div>
<div class="restaurant__ins-2 pt-20">
   <div class="col-xs-6">
      <div class="basket__info">
         <span>Итого с доставкой:</span>
      </div>
   </div>
   <div class="col-xs-6">
      <div class="bold text-right">
         <span class="">174 000 сум</span>
      </div>
   </div>   
   <div class="clearfix"></div>
   <div class="col-sm-12 text-center">
   <button class="mt-30 basket__btn">Оформить заказ</button>
   </div>         
</div>
</div>
   @include('partials.footer')
   @include('partials.footermobile')
   </div>
   <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
      crossorigin="anonymous"></script>
   @yield('scripts')
</body>

</html>
