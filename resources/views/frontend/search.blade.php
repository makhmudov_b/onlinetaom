<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        @include('partials.navdesktop')
        <div class="favorite-food search-result">
            <div class="container relative">
                <label for="search-res">
                    <img src="{{ asset('img/searchgreen.svg') }}" alt="">                
                </label>
                <input type="text" id="search-res">
                <div class="delete">
                    <img src="{{ asset('img/delete.svg') }}" alt="">                                    
                </div>
            </div>
        </div>        
        <div class="favorite desktop">
            <button class="favorite__btn">Рестораны</button>
            <button class="favorite__btn active">Блюда</button>
        </div>
        <div class="restaurant">
        <div class="container pt-20">
            <div class="flexbox">
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__delete">
                    <img src="{{ asset('img/brand1.png') }}" width="60" class="restaurant__brand">
                    Стейк Тибон
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop">4.2 км</p>
                </div>           
            </div>   
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__delete">
                    <img src="{{ asset('img/brand1.png') }}" width="60" class="restaurant__brand">
                    Стейк Тибон
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop">4.2 км</p>
                </div>           
            </div>   
            <div class="restaurant__ins--desktop">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__delete">
                    <img src="{{ asset('img/brand1.png') }}" width="60" class="restaurant__brand">
                    Стейк Тибон
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long-desktop">4.2 км</p>
                </div>           
            </div>  
            </div>
            </div>                                         
        </div>           
        @include('partials.footer')
        <div class="searching mobile">
            <label for="get">
                <img src="{{ asset('img/searchgreen.svg') }}" alt="">
            </label>
            <input type="search" id="get">
            <button class="searching__x">
                <img src="{{ asset('img/close.svg') }}">                    
            </button>            
        </div>
        <div class="favorite mobile">
            <button class="favorite__btn">Рестораны</button>
            <button class="favorite__btn active">Блюда</button>
        </div>
        <div class="restaurant mobile">
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__delete">
                    <img src="{{ asset('img/brand1.png') }}" width="60" class="restaurant__brand">
                    Стейк Тибон
                    <button class="restaurant__x">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>   
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__delete">
                    <img src="{{ asset('img/brand1.png') }}" width="60" class="restaurant__brand">
                    Стейк Тибон
                    <button class="restaurant__x">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>   
            <div class="restaurant__ins">
                <img src="{{ asset('img/mobile1.png') }}" class="restaurant__img">
                <div class="restaurant__delete">
                    <img src="{{ asset('img/brand1.png') }}" width="60" class="restaurant__brand">
                    Стейк Тибон
                    <button class="restaurant__x">
                        <img src="{{ asset('img/delete.svg') }}">                    
                    </button>
                </div>
                <div class="restaurant__line"></div>
                <div class="restaurant__ins-p">
                    <h5 class="restaurant__title">Caffee’issimo</h5>
                    <p class="restaurant__desc">Европейская</p>
                </div>
                <div class="restaurant__ins-price">
                <div class="restaurant__price"><img src="{{ asset('img/popularmob.svg') }}" height="10"> 12 000 сум</div> 
                    <p class="restaurant__long">4.2 км</p>
                </div>           
            </div>                                           
        </div>        
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        @yield('scripts')
    </body>
</html>
