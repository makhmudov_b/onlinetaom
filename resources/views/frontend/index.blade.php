@extends('layouts.frontend')
@section('content')
<div class="search desktop">
    <div class="container cont-fix">
        <div class="search__text">Мой адрес доставки:</div>
        <div class="search__wrapper">
            <input type="text" class="search__input" placeholder="Мой адрес доставки:">
            <button class="search__btn"><img src="{{ asset('img/location.svg') }}"></button>
        </div>
        <button class="search__confirm">Подтвердить</button>
    </div>
</div>
<div class="mobnav__bottom mobile">
    <div class="text-center">
        <img src="{{ asset('img/loc.svg') }}">
        Юнусабад, ул. Кашгар, д. 32
    </div>
</div>
<section class="slider">
    <div class="slider-for">
        <div class="slider__inner">
        </div>
        <div class="slider__inner">
        </div>
    </div>
    <div class="slider-nav">
        <div class="slide-nav__inner">
            <div class="container text-center">
                <div class="slider__text">
                    april restaurant
                </div>
                <div class="slider__title">
                    Закажи стейк, получи подарок от шефа!
                </div>
                <div class="slider__date">
                    до 18 марта, 2019
                </div>
            </div>
        </div>
        <div class="slide-nav__inner">
            <div class="container text-center">
                <div class="slider__text">
                    Issimo restaurant
                </div>
                <div class="slider__title">
                    Закажи стейк, получи подарок от Issimo!
                </div>
                <div class="slider__date">
                    до 18 February, 2019
                </div>
            </div>
        </div>
    </div>
</section>
<section class="rest-nav mobile">
    <div class="rest-nav__inner">
        <img src="{{ asset('/img/prod1.png') }}" class="rest-nav__img">
        <p class="rest-nav__p">Азиатская</p>
    </div>
    <div class="rest-nav__inner">
        <img src="{{ asset('/img/prod2.png') }}" class="rest-nav__img">
        <p class="rest-nav__p">Национальная</p>
    </div>
    <div class="rest-nav__inner">
        <img src="{{ asset('/img/prod3.png') }}" class="rest-nav__img">
        <p class="rest-nav__p">Пицца</p>
    </div>
    <div class="rest-nav__inner">
        <img src="{{ asset('/img/prod4.png') }}" class="rest-nav__img">
        <p class="rest-nav__p">Бургеры</p>
    </div>
    <div class="rest-nav__inner">
        <img src="{{ asset('/img/prod5.png') }}" class="rest-nav__img">
        <p class="rest-nav__p">Европейская</p>
    </div>
</section>
@include('partials.type')
@include('partials.popular')
@include('frontend.mobile')
@include('partials.download')
@include('partials.text')
@endsection
@section('script')
<script>
$('.slider-for').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots:true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    fade:true
});
</script>
@endsection