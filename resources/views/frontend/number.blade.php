<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OnlineTaom</title>
    <link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
</head>
    <body>
        <div class="events mobile">
            <div class="container">
                <button class="events__arrow">
                    <img src="{{ asset('img/arrowgreen.svg') }}">
                </button>
                <div class="text-center">
                    Вход
                </div>
            </div>
        </div>
        <div class="verify">
            <div class="text-center">
                <img src="{{ asset('img/phone.svg') }}" class="verify__phone">
                <div class="verify__sms">
                    Мой номер
                </div>
            </div>
            <div class="verify__input">
               <label for="tel">+998</label><input type="tel" id="tel">
            </div>
            <div class="verify__about">
                    Нажимая на кнопку “Получить код”, <a href="">я соглашаюсь с условиями использования приложения</a>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>            
        <script src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/3/jquery.inputmask.bundle.js"></script>
        <script>
            $("#tel").inputmask({"mask": "(99) 999-99-99"});
        </script>
    </body>
</html>
