@extends('layouts.frontend')
@section('content')
<div class="filter">
    <div class="filter__title">
        Стоимость блюд
    </div>
    <div class="filter__info">
        до <span class="fitler__price">480 000</span> сум
    </div>
    <div class="filter__range range-slider">
        <input type="text" class="js-range-slider" value="" />
    </div>
    <div class="filter__title">
        Время приготовления
    </div>
    <div class="filter__info">
        до <span class="filter__time">40</span> минут
    </div>
    <div class="filter__range range-slider">
        <input type="text" class="js-range-slider-2" value="" />
    </div>    
</div>
<div class="description-choice">
    <div class="container">
        <div class="description-choice__title">
            Категория
        </div>
        <div class="description-choice__type">
            <input type="radio" name="size" id="size_1" value="small" checked />
            <label for="size_1">Все</label>

            <input type="radio" name="size" id="size_2" value="small" />
            <label for="size_2">Национальная</label>

            <input type="radio" name="size" id="size_3" value="small" />
            <label for="size_3">Европейская</label>

            <input type="radio" name="size" id="size_4" value="small" />
            <label for="size_4">Азиатская</label>

            <input type="radio" name="size" id="size_5" value="small" />
            <label for="size_5">Пицца</label>

            <input type="radio" name="size" id="size_6" value="small" />
            <label for="size_6">Бургеры</label>                    
        </div>                     
        <div class="description-choice__line"></div>
        <div class="description-choice__title">
            Бесплатная доставка
        </div>
        <div class="description-choice__type">
            <input type="radio" name="shipping" id="ship_1" value="small" checked />
            <label for="ship_1">да</label>
            <input type="radio" name="shipping" id="ship_2" value="small" />
            <label for="ship_2">не важно</label>                    
        </div>
    </div>
    <div class="description-choice__bottom">
        Применить фильтры    
    </div>          
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>
<script>
$(".js-range-slider").ionRangeSlider({
    skin: "square",
    type: "double",
    min: 0,
    // hide_min_max:true,
    hide_from_to:true,
    max: 1000000,
    from: 0,
    to: 480000,
    postfix: " Сум"
});
$(".js-range-slider-2").ionRangeSlider({
    skin: "square",
    type: "double",
    min: 0,
    // hide_min_max:true,
    hide_from_to:true,
    max: 90,
    from: 0,
    to: 40,
    postfix: " мин"
});
if (e.cancelable) {
   e.preventDefault();
}
</script>
@endsection