<table class="table card-table table-vcenter text-nowrap">
        <thead>
        <tr>
            <th class="w-1">No</th>
            <th>Дата</th>
            <th class="w-1">Пользователь</th>
            <th>Ресторан</th>
            <th>Статус</th>
            <th>Ф\О</th>
            <th>Тип заказа</th>
            <th>Ответ</th>
            <th>Сумма</th>
            <th>Доставка</th>
            <th>Общая сумма</th>
        </tr>
        </thead>
        <tbody>
        @foreach($data as $datas)
        <tr>
            <td>#{{ $datas->id }}</td>
            <td>{{ \Carbon\Carbon::parse($datas->updated_at)->isoFormat('D MMMM HH:mm') }}</td>
            <td>@if($datas->user){{ $datas->user->phone }}@endif</td>
            @if($datas->restaurant !== null)
            <td>{{ $datas->restaurant->name }}</td>
            @else
            <td>Не выбран</td>
            @endif
            @if($datas->status == -1)
                <td>Отменен</td>
            @endif
            @if($datas->status == 0)
                <td>В корзине</td>
            @endif
            @if($datas->status == 1)
                <td>Оформлен</td>
            @endif
            @if($datas->status == 2)
                <td>Принят</td>
            @endif
            @if($datas->status == 3)
                <td>Оплачен</td>
            @endif
            @if($datas->status == 4)
                <td>Доставлен</td>
            @endif
            @if($datas->status != 0)
                @if($datas->payment_method == 0)
                    <td>Наличные</td>
                @else
                    <td>Payme</td>
                @endif
            @else
            <td>Не выбрано</td>
            @endif
            <td>{{ $datas->shipping_time !== '0' ? $datas->shipping_time : 'Не указано'  }}</td>
            <td @if($datas->status == 1) class="time" data-updated="{{ ($datas->updated_at)->Format('D M d Y H:i:s e O T') }}" @endif>
                @if($datas->status == 0)
                    Не заказан
                @endif
                @if($datas->status == 1)
                      Отвечен
                @endif
                @if($datas->status == 2)
                      Отвечен
                @endif
                @if($datas->status == 3)
                      Отвечен
                @endif
                @if($datas->status == 4)
                      Отвечен
                @endif
            </td>
            <td>{{ number_format($datas->total_price - $datas->shipping_price) }}</td>
            <td>{{ number_format($datas->shipping_price) }}</td>
            <td>{{ number_format($datas->total_price) }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
