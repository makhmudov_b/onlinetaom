import React, { Component } from 'react'
import Header from './components/Header'
import Footer from './components/Footer'
import Map from './components/Map'

import Bask from './containers/Bask';
import Home from './containers/Home';
import Event from './containers/Event';
import About from './containers/About';
import Categorie from './containers/Categorie';
import FavRest from './containers/FavRest';

import History from './containers/History';
import Info from './containers/Info';
import Mark from './containers/Mark';
import Number from './containers/Number';
import Payment from './containers/Payment';
import Profile from './containers/Profile';
import RestDesc from './containers/RestDesc';
import RestInner from './containers/RestInner';
import Find from './containers/Find';
import Verify from './containers/Verify';
import Login from './containers/Login';

import NotFound from './containers/NotFound';
import axios from 'axios';

import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Modal from 'react-modal';

Modal.setAppElement(document.getElementById('modal'));

const customStyles = {
    content : {
        top                   : '0%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : '0',
        transform             : 'translate(-50%, 0%)',
        position              : 'fixed',
        overflowY             : 'auto',
        overflowX             : 'hidden',
        padding               : 0,
        background            : 'transparent',
        border                : 'none',
        borderRadius          : '0',
        width                 : '100%',
    }
};

export default class App extends Component {

    constructor(props){
        super(props);

        this.state={
            cart: {
                totalItems: null,
                totalPrice: null,
                latitude: null,
                longitude: null,
                address: null,
                isLoaded: false,
                restId: null,
                ready: false
            },
            searchData: null,
            keyword: null,
            isLoaded: false,
            modalIsOpen: false
        }
    }
    componentDidMount(){
        if(localStorage.getItem('token') !== null){
            this.checkToken();
            setTimeout(this.updateCart ,500);
        }
            setTimeout(this.getMyLocation,200);
        let location = localStorage.getItem('location');
        if(location !== null && JSON.parse(location).long !== 0){
            let el = JSON.parse(localStorage.getItem('location'));
            this.createNotification('error');
            this.postLocation(el.long,el.lat)
        }
        else{
            this.setState({
                modalIsOpen: true
            });
        }
    }

    checkToken = () => {
        axios
            .get(`https://onlinetaom.uz/api/user`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                }
            )
            .catch((err) => {
                localStorage.removeItem('token');
                this.refresh();
            });
    }
    pageLoaded = () => {
        this.setState(prevState => ({
            ...prevState,
            isLoaded: true
        }));
    }
    runSearch = (data , keyword) => {
        this.setState(prevState => ({
            ...prevState,
            searchData: data,
            keyword : keyword
        }));
    }
    refresh = () => {
        window.location.reload();
    }
    getMyLocation = () => {
        let options = {
          enableHighAccuracy: false,
          timeout: 10000,
          maximumAge: 0
        };
        navigator.geolocation.getCurrentPosition((position) => {
            if(position.coords.longitude && position.coords.latitude){
                    this.saveLocation(position.coords.longitude, position.coords.latitude);
            }
        },
        (err) => {
            let location = {
                long: 69.240562,
                lat: 41.311081
            };
            this.saveLocation(location.long, location.lat);
        localStorage.setItem('location',JSON.stringify(location));

        }, options);
    }
    saveLocation = (long , lat) => {
        this.setState(prevState => ({
            ...prevState,
            cart: {
                ...prevState.cart,
                latitude: lat,
                longitude: long,
            }
        }));
    }
    postLocation = (long , lat) => {
        let location = {
            long: long,
            lat: lat,
        };
        localStorage.setItem('location',JSON.stringify(location))

        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.post("https://onlinetaom.uz/api/location",
            {
                    long: long,
                    lat: lat
                    },
             headers)
        .then(result => {
        let  data  = result.data.properties;
        let sub    = data.subLocality !== undefined ? data.subLocality : '';
        let name   =  data.streetName !== undefined ? data.streetName : '';
        let number = data.streetNumber !== undefined ? data.streetNumber : '';
        this.setState(prevState => ({
            ...prevState,
            cart: {
                ...prevState.cart,
                latitude: lat,
                longitude: long,
                address:  sub +" "+ name +" "+ number,
                ready: true
            }
        }));
        })
        .catch((err) => {
            // console.warn(err);
        })
    }
    createNotification = (type) => {
        switch (type) {
        case 'info':
            return NotificationManager.info('Можно добавлять только с одного ресторана');
            break;
        case 'minimum':
            return NotificationManager.info('Минимальная сумма заказа состовляет: 30 000 сум');
        break;
        case 'success':
            return NotificationManager.success('Успешно!');
            break;
        case 'warning':
            return NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);
            break;
        case 'error':
            return NotificationManager.info('Измените вашу локацию если она указана неправильно',' ',8000);
            break;
        }
    };
    updateCart = (item, price) => {
        axios
            .get(`https://onlinetaom.uz/api/basket`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                this.setState(prevState => ({
                    ...prevState,
                    cart: {
                        ...prevState.cart,
                        totalItems: parseInt(result.data.amount , 10),
                        totalPrice: parseInt(result.data.price , 10),
                        restId: parseInt(result.data.restId , 10)
                    }
                }));
                }
            )
            .catch((err) => {
                // console.log(err);
            });
    }
    togglePopup = () => {
        this.header.toggle()
    }
    openModal = () => {
        this.setState(prevState => ({
            ...prevState,
            modalIsOpen: true,
            cart: {
                ...prevState.cart,
            }
        }))
    }

    closeModal = () => {
        this.setState(prevState => ({
            ...prevState,
            modalIsOpen: false,
            cart: {
                ...prevState.cart,
            }
        }))
    }


    render(){
        if(this.state.isLoaded || true){
        return (
            <div>
                <Router>
                    <Header onRef={ref => (this.header = ref)}  cart={this.state.cart} runSearch={(data , keyword) => this.runSearch(data , keyword)} searchData={this.state.searchData} refresh={this.refresh} />
                    <NotificationContainer />
                    <Modal id="modal"
                           ariaHideApp={false}
                           isOpen={this.state.modalIsOpen}
                           onRequestClose={this.closeModal}
                           style={customStyles}
                           shouldCloseOnOverlayClick={false}
                           overlayClassName="modal-bg"
                        >
                        <Map { ... this.state.cart } myLocation={this.getMyLocation} hide={this.closeModal} saveLocation={this.postLocation} />
                    </Modal>
                    <Switch>
                        <Route exact path="/" render={(props)=><Home {...props} notify={this.createNotification} openModal={this.openModal} cart={this.state.cart}  saveLocation={this.postLocation} />} />
                        <Route path="/auth" component={Login} />
                        <Route path="/basket" render={(props)=><Bask {...props} notify={this.createNotification} toggleMap={this.openModal} price={this.state.cart.totalPrice} long={this.state.cart.longitude} lat={this.state.cart.latitude}  address={this.state.cart.address} update={this.updateCart}/>} />
                        <Route path="/events" component={Event} />
                        <Route path="/about" component={About} />
                        <Route path="/favorites" component={FavRest} />
                        <Route path="/history" component={History} />
                        <Route path="/info" render={(props)=><Info notify={this.createNotification} />} />
                        <Route path="/mark" component={Mark} />
                        <Route path="/number" component={Number} />
                        <Route path="/order/:id" render={(props)=><Payment {...props} refresh={this.refresh}  />} />
                        <Route path="/profile" render={(props)=><Profile {...props} refresh={this.refresh}  />} />
                        <Route path="/restaurant/:alias/:id"render={(props)=><RestDesc {...props} check={this.state.cart.restId} notify={this.createNotification} update={this.updateCart}/>} />
                        <Route path="/restaurant/:alias" render={(props)=><RestInner {...props} togglePopup={this.togglePopup} notify={this.createNotification} restId={this.state.cart.restId} long={this.state.cart.longitude} lat={this.state.cart.latitude} update={this.updateCart}/>} />
                        <Route path="/search" render={(props)=> <Find data={this.state.searchData} keyword={this.state.keyword}/>} />
                        <Route path="/verify" render={(props)=><Verify {...props} refresh={this.refresh}  />} />
                        <Route path="/:alias" render={(props)=><Categorie  cart={this.state.cart} />} />
                        <Route component={NotFound} />
                    </Switch>
                    <Footer />
                </Router>
            </div>
        );
        }
        else{
            return null;
        }
    }
}
