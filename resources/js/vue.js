import './vue/bootstrap'
import Vue from 'vue'

import NotificationsDemo from './vue/components/NotificationsDemo'
import NotificationsDropdown from './vue/components/NotificationsDropdown'

new Vue({
  el: '#app',

  components: {
    NotificationsDemo,
    NotificationsDropdown
  }
})