import React, { Component } from 'react';
import RestMobileBox from '../RestMobileBox';

export default class RestMobile extends Component {
    render() {
        return (
<div className="restaurant mobile"> 
    <RestMobileBox />
    <RestMobileBox />
    <RestMobileBox />
    <RestMobileBox />
    <RestMobileBox />
    <RestMobileBox />
</div>    
        );
    }
}