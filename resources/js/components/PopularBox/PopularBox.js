import React, { Component } from 'react';
import MediaQuery from 'react-responsive';
import {Link} from "react-router-dom";
import axios from 'axios';
import Loader from 'react-loader-spinner';

export default class PopularBox extends Component {
    _isMounted = false;

    constructor(props){
        super(props);
        this.state={
            distance: null,
        }
    }
    componentDidMount() {
        this._isMounted = true;
        let destination = (JSON.parse(localStorage.getItem('location')));
        if(this.props.ready){
        if(destination !== null){
            if(this.props.longitude != destination.long){
            if(localStorage.getItem('restaurant'+this.props.id) !== null){
            this.setState({
                distance: JSON.parse(localStorage.getItem('restaurant'+this.props.id)).distance
            })}
            } else{
                this.getDistance();
                }
            }
        }
    }
    getDistance = () => {
        let destination = (JSON.parse(localStorage.getItem('location')));
        let data = {
            restaurantId:this.props.id,
            origin: this.props.lat + ',' + this.props.long,
            destination: destination.lat + ',' + destination.long,
        };
        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.post("https://onlinetaom.uz/api/distance", data, headers)
        .then(result => {
            if(this._isMounted){
                this.setState({
                    distance: result.data
                },()=>localStorage.setItem('restaurant'+this.props.id,JSON.stringify({id:this.props.id, distance:result.data})));
            }},
            (error) => {
                this.setState({
                    isLoaded: true,
                    error
                });
            });
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.longitude && this.props.longitude ) {
            if(nextProps.ready){
                if (nextProps.longitude !== 69.240562) {
                    if(this.props.longitude  != nextProps.longitude){
                    let data = {
                        restaurantId:this.props.id,
                        origin: this.props.lat + ',' + this.props.long,
                        destination: nextProps.latitude + ',' + nextProps.longitude
                    };
                    let headers = {
                        headers: {
                            'Authorization': `Bearer ${localStorage.getItem('token')} }`
                        }
                    };
                    axios.post("https://onlinetaom.uz/api/distance", data, headers)
                        .then(result => {
                            if (this._isMounted) {
                                    this.setState({
                                        distance: result.data
                                    },()=>localStorage.setItem('restaurant'+this.props.id,JSON.stringify({id:this.props.id, distance:result.data}))
                                        );
                                }
                            },
                            (error) => {
                                this.setState({
                                    isLoaded: true,
                                    error
                                });
                            });
                }
                }
            }
        }
    }

componentWillUnmount() {
    this._isMounted = false;
}

    render() {
        return (
            <div key={this.props.id}>
                <MediaQuery maxWidth={768}>
                    <div className="restaurant">
                        <div className="restaurant--wrap">
                            <Link to={`/restaurant/${this.props.alias}`} className="restaurant__ins">
                                <img src={this.props.cover} className="restaurant__img" />
                                <div className="restaurant__ins-p text-left">
                                    <img src={this.props.image} width="60" className="restaurant__brand br50" />
                                    <h5 className="restaurant__title">{this.props.name}</h5>
                                    <p className="restaurant__desc">{this.props.categories.length>0?this.props.categories[0].name_ru:""}</p>
                                </div>
                                <div className="restaurant__ins-price">
                                        <div className="restaurant__price"><img src="/img/popularmob.svg" className={this.state.distance ? '' : 'none'} height="10" /> {this.state.distance ? this.state.distance.price + ' сум' : <Loader type="Watch" color="#000000" width="20" height="20"/>}</div>
                                    <p className="restaurant__long">{this.state.distance ? this.state.distance.distance : ''}</p>
                                </div>
                            </Link>
                        </div>
                    </div>
                </MediaQuery>
                <MediaQuery minWidth={768}>
                    <Link to={`/restaurant/${this.props.alias}`}>
                        <div className="popular__wrapper desktop" style={{background: `linear-gradient(180deg, rgba(0, 0, 0, 0) 45.86%, rgba(0, 0, 0, 0.75) 100%), url(${this.props.cover}) 50% no-repeat`,backgroundSize: 'cover'}}>
                            <div className="popular__wrapper-1">
                                <img src={this.props.image} width="60" className="popular__wrapper-img br50" style={{'vertical-align':'top'}} />
                                <div className="popular__wrapper-text">
                                    <h2 className="popular__title">{this.props.name}</h2>
                                    <p className="popular__p">{this.props.categories.length>0?this.props.categories[0].name_ru:""}</p>
                                </div>
                                <div className="popular__wrapper-price">
                                    <img src="/img/popular.svg" className={this.state.distance ? '' : 'none'} /><h2 className="popular__price">{this.state.distance ? this.state.distance.price + ' сум' : <Loader type="Watch" color="#FFFFFF" width="20" height="20"/>}</h2>
                                    <p className="popular__long pt-fix">{this.state.distance ? this.state.distance.distance : ''}</p>
                                </div>
                            </div>
                        </div>
                    </Link>
                </MediaQuery>
            </div>
        );
    }
}
