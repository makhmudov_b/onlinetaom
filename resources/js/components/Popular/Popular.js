import React, { Component } from 'react';
import Loader from 'react-loader-spinner'
import PopularBox from '../PopularBox';

let _sorting;
export default class Popular extends Component {

    constructor(props){
        super(props);

        this.state={
            error: null,
            isLoaded: false,
            items: [],
            sorting: [],
            start: 0,
        }
    }

    componentDidMount() {
        this.getRestaurant();
        setTimeout(this.sortByPriceAsc,5000);
    }
    // componentWillReceiveProps(nextProps, nextContext) {
    //     if(nextProps.longitude && this.props.longitude ) {
    //         if (nextProps.ready) {
    //             if (nextProps.longitude !== 0) {
    //                 this.getRestaurant();
    //             }
    //         }
    //     }
    // }

      sortByPriceAsc = () => {
        let sorted = this.state.sorting.sort((a, b) => ((a.distance.distance).replace(/\D/g, "") - (b.distance.distance).replace(/\D/g, "")))
        sorted  = sorted.map(({id}) => id);
        // let el = this.state.items.sort(function(a, b){
        //   return sorted.indexOf(a.id) - sorted.indexOf(b.id);
        // });
        // this.state.items.sort(function(obj1,obj2){
        //    return a.indexOf(obj1.id) > a.indexOf(obj2.id)
        // });

        let items = this.state.items.sort(function(a, b){
          return sorted.indexOf(a.id) - sorted.indexOf(b.id);
        });
        this.setState({
            items : items
        })
      }
    getRestaurant = () => {
        let el = JSON.parse(localStorage.getItem('location'));
        if(el !== null) {
            fetch("https://onlinetaom.uz/api/restaurants/"+this.state.start,
                {
                    headers: {
                        'location': el.lat + ',' + el.long,
                    }
                })
                .then(res => res.json())
                .then(
                    (result) => {
                        let data = result;
                        let sortItems = result.map(({id}) => id);
                        _sorting =  sortItems.map((item) => {
                            return JSON.parse(localStorage.getItem('restaurant'+item))
                        })

                        this.setState({
                            isLoaded: true,
                            items: [...this.state.items,...data],
                            sorting : _sorting
                        });
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        } else {
            fetch("https://onlinetaom.uz/api/restaurants/"+this.state.start)
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            isLoaded: true,
                            items: [...this.state.items,...result],
                        });
                    },
                    // Note: it's important to handle errors here
                    // instead of a catch() block so that we don't swallow
                    // exceptions from actual bugs in components.
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                )
        }
    }

    loadMore = () => {
        this.setState({
            start: this.state.start+15,
        },()=>this.getRestaurant());
    };

    render() {
        const { isLoaded, items } = this.state;
        if (!isLoaded) {
            return <div className="text-center">
                <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100" />
            </div>;
        } else {
            return (
                <section className="popular react-popular" style={{paddingBottom:'0'}}>
                    <div className="container cont-fix">
                        <h3 className="popular__h3 text-center desktop">Популярные рестораны</h3>
                        <div className="flex">

                            {items.map(item => (
                                <PopularBox {...item} key={item.id} longitude={this.props.longitude} latitude={this.props.latitude} ready={this.props.ready} />
                            ))}
                        </div>
                    </div>
                    {/*<button className="popular__btn" onClick={this.loadMore}>Показать еще</button>*/}
                </section>
            );
        }
    }
}
