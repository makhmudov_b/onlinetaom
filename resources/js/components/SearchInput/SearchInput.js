import React, { Component } from 'react';

export default class SearchInput extends Component {
    render() {
        return (
        <div className="favorite-food search-result">
            <div className="container relative">
                <label htmlFor="search-res">
                    <img src="/img/searchgreen.svg" />                
                </label>
                <input type="text" id="search-res" />
                <div className="delete">
                    <img src="/img/delete.svg" />
                </div>
            </div>
        </div>  
        );
    }
}
