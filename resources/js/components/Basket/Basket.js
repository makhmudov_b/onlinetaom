import React, { Component } from 'react';
import BasketInfo from '../BasketInfo';
import BasketItem from '../BasketItem';
import MediaQuery from 'react-responsive';
import moment from 'moment';
import DatePicker   from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import PropTypes from 'prop-types';


class CustomButton extends Component {
  render () {
    return (
    <button
        className="example-custom-input"
        onClick={this.props.onClick}>
        {this.props.value}
      </button>
    )
  }
}

CustomButton.propTypes = {
  onClick: PropTypes.func,
  value: PropTypes.string
};

export default class Basket extends Component {
    _destination = JSON.parse(localStorage.getItem('location'));

    constructor(props) {
        super(props);

        this.state = {
            error: null,
            isActive : 'fast',
            time: moment().add(2, 'hours').toDate()
        };
    }
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    toggleActive = (event) => {
        let target = event.target.id;
        this.setState(prevState => ({
            ...prevState,
            isActive : target
        }));
        if(target === 'fast'){
            this.props.changeTime('Быстрая доставка');
        }
        if(target === 'time'){
            this.props.changeTime(`Время доставки ${moment(this.state.time).format('HH:mm')}`);
        }
    }
    handleChange = (value) => {
        if(this.state.isActive === 'time'){
            this.setState({
                time: value
            },
            this.props.changeTime(`Время доставки ${moment(value).format('HH:mm')}`)
            )
        }
    }
    onComment = (event) => {
        this.props.addComment(event.target.value)
    }
    componentWillReceiveProps(nextProps, nextContext) {
        if(this._destination.lat != nextProps.lat && this._destination.long != nextProps.long){
            if(this.props.basket !== null){
                this.props.getDistance(this.props.basket)
            }
        }
    }

    render() {
        if(this.props.basket ){
            if(this.props.basket.restaurant_id === 0){
                return (
            <div>
                <section className="basket desktop">
                    <div className="basket__title">
                        <img src="/img/cart-green.svg" />
                        <h2>
                            Моя корзина
                        </h2>
                    </div>
                </section>
                <section className="events mobile">
                    <div className="container">
                        <div className="text-center">
                            Моя корзина
                        </div>
                    </div>
                </section>
                <section className="basket-back">
                    <div className="container cont-fix cont-mobile">
                        <MediaQuery maxWidth={768}>
                            <div className="restaurant__ins-2 pt-20">
                                <div className="col-sm-12 text-center">
                                    <button className="basket__btn">Ваша корзина пуста</button>
                                </div>
                            </div>
                        </MediaQuery>
                        <div className="item__checkout small-hidden">
                            <div className="col-md-12 col-sm-12 col-xs-12 text-center">
                                <div className="basket__btn">Ваша корзина пуста</div>
                            </div>

                            <div className="clearfix"></div>
                        </div>
                    </div>
                </section>
            </div>
                );
            }
            return (
            <div>
                <section className="basket desktop">
                    <div className="basket__title">
                        <img src="/img/cart-green.svg" />
                        <h2>
                            Моя корзина
                        </h2>
                    </div>
                    <BasketInfo { ... this.props.basket.restaurant  } distance={this.props.distance} />
                </section>
                <section className="events mobile">
                    <div className="container">
                        <div className="text-center">
                            Моя корзина
                        </div>
                    </div>
                </section>
                <div className="basket mobile">
                    <div className="restaurant__more">
                        <img src={this.props.basket.restaurant.image} className="vm" width="60" /> { this.props.basket.restaurant.name }
                        <div className="restaurant__more-inner">
                            <div className="restaurant__price"><img src="/img/popularmob.svg" height="10" /> {this.props.distance ? this.props.distance.price : 0} сум</div>
                            <p className="restaurant__long pt-5">{this.props.distance ? this.props.distance.distance : 0}</p>
                        </div>
                    </div>
                </div>
                <section className="basket-back">
                    <div className="container cont-fix cont-mobile">
                        {this.props.basket.order_food.map(item => (
                            <BasketItem updated={this.props.updated} {...item} key={item.id} />
                        )) }
                        <div className="restaurant__ins-2 small-show">
                            <div className="col-xs-12 text-left">
                                <div className="item__checkout--info" style={{border: '1px solid red',cursor:'pointer'}} onClick={this.props.toggleMap}>
                                    <input type="text" className="input" style={{ width: '300px'}} value={this.props.address ? this.props.address : ''} disabled={true} placeholder="Укажите адрес доставки" />
                                    <img src="/img/loc-green.svg" width="15" />
                                </div>
                                <div className="item__checkout--info">
                                    <button className={`button ${this.state.isActive === 'fast' ? 'active' : '' }`} id="fast" onClick={this.toggleActive} >Срочная доставка</button>
                                    <button className={`button ${this.state.isActive === 'time' ? 'active' : '' }`} id="time" onClick={this.toggleActive} >На время</button>
                                        <div className={`mt-10 pl-10 ${this.state.isActive === 'time' ? '' : 'small-hidden'}`}>
                                            <label>Укажите время: </label>
                                            <DatePicker
                                                selected={this.state.time}
                                                onChange={this.handleChange}
                                                minTime={moment().add(2,'hours').toDate()}
                                                maxTime={moment().endOf('day').toDate()}
                                                customInput={<CustomButton/>}
                                                showTimeSelect
                                                showTimeSelectOnly
                                                timeIntervals={15}
                                                dateFormat="HH:mm"
                                                timeFormat="HH:mm"
                                                timeCaption="Time"
                                            />
                                        </div>
                                    </div>
                                <div className="item__checkout--info">
                                    <textarea rows="4" onChange={this.onComment} placeholder="Комментарий к заказу"></textarea>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <MediaQuery maxWidth={768}>
                            <div className="restaurant__ins-2 pt-20">
                                <div className="col-xs-6">
                                    <div className="basket__info">
                                        <span>Итого с доставкой:</span>
                                    </div>
                                </div>
                                <div className="col-xs-6">
                                    <div className="bold text-right">
                                        <span className="">{ this.props.distance ? this.props.price + parseInt((this.props.distance.price).replace(/ /g,'')) : 0} сум</span>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                                <div className="col-sm-12 text-center">
                                    <button onClick={this.props.price > 30000 ? (auth) => this.props.forwardTo('check') : (type) => this.props.notify('minimum')} className="mt-30 basket__btn">Оформить заказ</button>
                                </div>
                            </div>
                        </MediaQuery>
                        <div className="item__checkout small-hidden">
                            <div className="col-md-5 col-sm-6 col-xs-6 text-left">
                                <div className="item__checkout--info" style={{border: '1px solid red',cursor:'pointer'}} onClick={this.props.toggleMap}>
                                    <input type="text" className="input" value={this.props.address ? this.props.address : ''} disabled={true} placeholder="Укажите адрес доставки" />
                                    <img src="/img/loc-green.svg" width="15" />
                                </div>
                                <div className="item__checkout--info">
                                    <button className={`button ${this.state.isActive === 'fast' ? 'active' : '' }`} id="fast" onClick={this.toggleActive} >Срочная доставка</button>
                                    <button className={`button ${this.state.isActive === 'time' ? 'active' : '' }`} id="time" onClick={this.toggleActive} >На время</button>
                                        <div className={`mt-10 pl-10 ${this.state.isActive === 'time' ? '' : 'dispnone'}`}>
                                            <label>Укажите время: </label>
                                            <DatePicker
                                                selected={this.state.time}
                                                onChange={this.handleChange}
                                                minTime={moment().add(2,'hours').toDate()}
                                                maxTime={moment().endOf('day').toDate()}
                                                customInput={<CustomButton/>}
                                                showTimeSelect
                                                showTimeSelectOnly
                                                timeIntervals={15}
                                                dateFormat="HH:mm"
                                                timeFormat="HH:mm"
                                                timeCaption="Time"
                                            />
                                        </div>
                                    </div>
                                <div className="item__checkout--info">
                                    <textarea rows="4" onChange={this.onComment} placeholder="Комментарий к заказу"></textarea>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        <div className="item__checkout small-hidden">
                            <div className="col-md-6 col-sm-6 col-xs-6 text-left">
                                <button className="basket__btn" onClick={this.props.price > 30000 ? (auth) => this.props.forwardTo('check') : (type) => this.props.notify('minimum') }>Оформить заказ</button>
                            </div>
                            <div className="col-md-6 col-sm-6 text-right">
                                <div className="basket__info">
                                    <span>Итого с доставкой:</span>
                                    <span className="price">{ this.props.price && this.props.distance !== null ? this.props.price + parseInt((this.props.distance.price).replace(/ /g,'')) : 0} сум</span>
                                </div>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
    else{
            return null ;
        }
    }

}
