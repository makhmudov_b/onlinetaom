import React, { Component } from 'react';
import Location from '../Location';

export default class Search extends Component {
    render() {
        return (
<div>
<div className="search desktop text-center">
    <div className="container cont-fix">
        <div className="search__text">Мой адрес доставки:</div>
        <div className="search__wrapper">
            <input type="text" disabled={true} className="search__input" defaultValue={this.props.address} placeholder="Мой адрес доставки:" />
            <button className="search__btn" onClick={this.props.open}><img src="/img/location.svg" /></button>
        </div>
    </div>
</div>
<Location address={this.props.address} open={this.props.open}/>
</div>
        );
    }
}
