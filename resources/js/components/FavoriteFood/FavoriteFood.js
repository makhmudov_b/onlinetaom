import React, { Component } from 'react';
import axios from 'axios';

export default class FavoriteFood extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            distance: null,
        }
    }
    componentDidMount() {
        this.getDistance();
    }
    getDistance = () => {
        let destination = (JSON.parse(localStorage.getItem('location')));
        let data = {
            restaurantId: this.props.restaurant.id,
            origin: this.props.restaurant.lat + ',' + this.props.restaurant.long,
            destination: destination.lat + ',' + destination.long
        };
        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.post("https://onlinetaom.uz/api/distance", data, headers)
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        distance: result.data,
                    }));
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
    render() {
        return (
            <div>
                <div className="restaurant__ins--desktop desktop">
                    <img src={this.props.image} className="restaurant__img" />
                    <button className="restaurant__x" style={{top: '10px', right:'10px'}} onClick={this.props.delete} id={this.props.deleteId}>
                        <img src="/img/x.svg" id={this.props.deleteId} />
                    </button>
                    <div className="restaurant__delete">
                        <img src={this.props.restaurant.image} className="restaurant__brand br50" width="60" />
                        {this.props.name_ru}
                    </div>
                    <div className="restaurant__line"></div>
                    <div className="restaurant__ins-p">
                        <h5 className="restaurant__title">{this.props.restaurant.name}</h5>
                        <p className="restaurant__desc">{this.props.restaurant.categories.length > 0 ? this.props.restaurant.categories[0].name_ru : ""}</p>
                    </div>
                    <div className="restaurant__ins-price">
                    <div className="restaurant__price"><img src="/img/popularmob.svg" height="10" /> {this.state.distance ? `${this.state.distance.price} сум` : 'Вычесление ...'}</div>
                        <p className="restaurant__long-desktop">{this.state.distance ? `${this.state.distance.distance}` : 'Вычесление ...'}</p>
                    </div>           
                </div>                
            <div className="restaurant mobile">
                <div className="restaurant__ins">
                    <img src={this.props.image} className="restaurant__img" />
                    <div className="restaurant__delete">
                        <img src={this.props.restaurant.image} className="restaurant__brand br50" width="60" />
                        {this.props.name_ru}
                        <button className="restaurant__x" onClick={this.props.delete} id={this.props.deleteId}>
                            <img src="/img/delete.svg" />                    
                        </button>
                    </div>
                    <div className="restaurant__line"></div>
                    <div className="restaurant__ins-p">
                        <h5 className="restaurant__title">{this.props.restaurant.name}</h5>
                        <p className="restaurant__desc">{this.props.restaurant.categories.length > 0 ? this.props.restaurant.categories[0].name_ru : ""}</p>
                    </div>
                    <div className="restaurant__ins-price">
                    <div className="restaurant__price"><img src="/img/popularmob.svg" height="10" /> {this.state.distance ? `${this.state.distance.price} сум` : 'Вычесление ...'}</div>
                        <p className="restaurant__long">{this.state.distance ? `${this.state.distance.distance}` : 'Вычесление ...'}</p>
                    </div>           
                </div>   
            </div>            
            </div>            
        );
    }
}
