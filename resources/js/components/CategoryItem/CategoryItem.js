import React, { Component } from 'react';
import Loader from 'react-loader-spinner'
import PopularBox from '../PopularBox';

export default class CategoryItem extends Component {

    constructor(props){
        super(props);

        this.state={
            error: null,
            isLoaded: false,
            items: []
        }
    }

    componentDidMount() {
        fetch(`https://onlinetaom.uz/api${window.location.pathname}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    componentWillReceiveProps(nextProps){
        fetch(`https://onlinetaom.uz/api${window.location.pathname}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, items } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div className="text-center">
                <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100" />
            </div>;
        } else {
            return (
                <section className="popular react-popular" style={{background :'#F0F4F5',paddingBottom: '20px'}}>
                    <div className="container cont-fix">
                        <h3 className="popular__h3 text-center desktop">Популярные рестораны</h3>
                        <div className="flex">
                            {this.state.items[0].restaurants.map(item => (
                                <PopularBox {...item} key={item.id} ready={this.props.ready} longitude={this.props.longitude} latitude={this.props.latitude} />
                            ))}
                        </div>
                        <button className="popular__btn">Показать еще</button>
                    </div>
                </section>
            );
        }
    }
}
