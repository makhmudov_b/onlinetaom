import React, { Component } from 'react';
import {Link} from "react-router-dom";

export default class Order extends Component {
    render() {
        return (
        <Link to={{ pathname: `/order/${this.props.id}` }}>
        <div className="restaurant small-hidden">
            <div className="container cont-fix">
            <div className="restaurant-desktop__ins">
                <div className="history-desktop">
                    <div className="aside aside-1">
                        <div className="history__ins-p">
                             №{this.props.id}
                        </div>
                    </div>
                    <div className="main">
                        <div className="vm">
                            <img src={this.props.restaurant ? this.props.restaurant.image : "/img/brand.png"} width="70" className="vm br50" />
                            <h5 className="restaurant__title pl-10">{this.props.restaurant ? this.props.restaurant.name : ``}</h5>
                            <table className="history__info-desk">
                            <tbody>
                            {this.props.order_food ? this.props.order_food.map(item => (
                                    <tr key={item.id}>
                                        <td>
                                            <span className="dots">
                                                {item.food.name_ru}
                                            </span>
                                        </td>
                                        <td>
                                            {item.food.price} сум
                                        </td>
                                        <td>
                                            {item.amount} шт
                                        </td>
                                    </tr>
                                )) : ''}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="foot text-right">
                        <div className="history__ins-price">
                            {this.props.total_price + ' сум' }
                        </div>
                        <div className="history__status">
                        {this.props.status == -1 ? `Отменен `: ''}
                        {this.props.status == 0 ? `В корзине `: ''}
                        {this.props.status == 1 ? `Оформлен `: ''}
                        {this.props.status == 2 ? `Принят`: ''}
                        {this.props.status == 3 ? `В пути `: ''}
                        {this.props.status == 4 ? `Доставлен `: ''}
                        {`(${this.props.updated_at})`}
                        </div>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        </div>
        </div>
        <div className="restaurant small-show">
            <div className="restaurant__ins">
                <div className="history">
                    <div className="restaurant__ins-p vm">
                        <img src={this.props.restaurant ? this.props.restaurant.image : "/img/brand.png"} className="vm br50" width="70" />
                        <h5 className="restaurant__title pl-10" style={{fontSize:'10px'}}>{this.props.restaurant ? this.props.restaurant.name : ''}</h5>
                    </div>
                    <div className="restaurant__ins-price">
                        <div className="restaurant__inner"> №{this.props.id}</div>
                    </div>
                </div>
                <div className="text-center">
                 {`(${this.props.updated_at})`}
                </div>
                <div className="history__info">
                    <table className="history__info-desk">
                        <tbody>
                        {this.props.order_food ? this.props.order_food.map(item => (
                            <tr key={item.id}>
                                <td>
                                    {item.food.name_ru}
                                </td>
                                <td>
                                    {item.food.price} сум
                                </td>
                                <td>
                                    {item.amount} шт
                                </td>
                            </tr>
                        )) : ''}
                        </tbody>
                    </table>
                </div>
                <div className="restaurant__line mt-10"></div>
                <div className="history__status">
                    <div className="col-xs-6 bold">
                        {this.props.status == -1 ? `Отменен `: ''}
                        {this.props.status == 0 ? `В корзине `: ''}
                        {this.props.status == 1 ? `Оформлен `: ''}
                        {this.props.status == 2 ? `Принят`: ''}
                        {this.props.status == 3 ? `В пути `: ''}
                        {this.props.status == 4 ? `Доставлен `: ''}
                    </div>
                    <div className="col-xs-6 text-right">
                        {this.props.total_price + ' сум' }
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        </div>
        </Link>
        );
    }
}
