import React, { Component } from 'react';

export default class Download extends Component {
    render() {
        return (
<section className="categories-info">
    <div className="container cont-fix">
        <div className="col-md-12 index">
            <h2 className="categories-info__h2">А в приложении все еще удобнее</h2>
        </div>
         <div className="col-md-3 lg-hidden index text-center">
            <button className="useful__btn"><img src="/img/playmarket.svg" />Загрузить для Android</button>
         </div>
         <div className="col-lg-6 col-md-12 index text-center">
            <div className="categories-info__phone">
               <img src="/img/phone1.png" />
            </div>
            <div className="categories-info__phone">
               <img src="/img/phone2.png" />
            </div>
            <button className="useful__btn dispnone"><img src="/img/playmarket.svg" />Загрузить для Android</button>            
            <button className="useful__btn mb-20 dispnone"><img src="/img/appstore.svg" />Загрузить для ApPlE</button>
         </div>
         <div className="col-md-3 lg-hidden index text-center">
            <button className="useful__btn"><img src="/img/appstore.svg" />Загрузить для ApPlE</button>
         </div>
   <div className="categories-info__parallax">
      <div data-relative-input="true" id="scene">
         <div data-depth="0.1" className="categories-info__parallax-el categories-info__parallax-el-1 md-hidden"><img src="/img/paralax1.png" /></div>
         <div data-depth="0.1" className="categories-info__parallax-el categories-info__parallax-el-2 md-hidden"><img src="/img/paralax2.png" /></div>
         <div data-depth="0.1" className="categories-info__parallax-el categories-info__parallax-el-3"><img src="/img/paralax3.png" /></div>
         <div data-depth="0.3" className="categories-info__parallax-el categories-info__parallax-el-4 md-hidden"><img src="/img/paralax4.png" /></div>
         <div data-depth="0.3" className="categories-info__parallax-el categories-info__parallax-el-5 md-hidden"><img src="/img/paralax5.png" /></div>
         <div data-depth="0.3" className="categories-info__parallax-el categories-info__parallax-el-6"><img src="/img/paralax6.png" /></div>
         <div data-depth="0.15" className="categories-info__parallax-el categories-info__parallax-el-7"><img src="/img/paralax7.png" /></div>
         <div data-depth="0.2" className="categories-info__parallax-el categories-info__parallax-el-8"><img src="/img/paralax8.png" /></div>
      </div>    
   </div>
</div>
</section>
        );
    }
}