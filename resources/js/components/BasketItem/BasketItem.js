import React, { Component } from 'react';
import axios from "axios/index";
import MediaQuery from 'react-responsive';

export default class BasketItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amount: 1,
        };
    }
    componentDidMount(){
        this.setState({
            amount : this.props.amount
        });
    }
    incrementAmount = () => {
        this.setState({
            amount : this.state.amount + 1,
        },
        this.update
        );
    }
    decrementAmount = () => {
        if(this.state.amount !== 1){
            this.setState({
                amount : this.state.amount - 1,

            },
            this.update
            );
        }
    }
    update = () => {
        let axiosConfig = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios
            .patch(`https://onlinetaom.uz/api/order/${this.props.id}`,{'amount':this.state.amount}, axiosConfig)
            .then(result => {
                this.props.updated();
            })
            .catch((err) => {
                console.log(err);
            })
    }
    delete = () => {
        let axiosConfig = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios
            .delete(`https://onlinetaom.uz/api/order/${this.props.id}`, axiosConfig)
            .then(result => {
               this.props.updated();
            })
            .catch((err) => {
                console.log(err);
            })
    }
    render() {
        return (
            <div>
<MediaQuery minWidth={768}>
<div className="item">
    <div className="item__text">
    <img src={this.props.food.image} width="100" />
    <div style={{display:'inline'}}>
        <h3>
        <span className="word-limit">
        {this.props.food.name_ru}
        </span>
        <div style={{paddingTop:'10px'}} className={this.props.size.length > 0 ? '' : 'display-none'}>
        Вид :
        {this.props.size.map((item, i) => (
            <React.Fragment key={item.id}>
                {i === 1 ? ','+item.name+ ' ':' '+item.name}
            </React.Fragment>
        )) }
        </div>
        <div style={{paddingTop:'10px'}} className={this.props.ids.length > 0  ? '' : 'display-none' }>Добавки :
        {this.props.ids.map((item, i) => (
            <React.Fragment key={item.id}>
                {i === 1 ? ','+item.name+ ' ':' '+item.name}
            </React.Fragment>
        )) }
        </div>
        </h3>
    </div>
    </div>
    <div className="item__p">
        {this.props.food.desc_ru}
    </div>
    <div className="item__num">
        <span className="input-number-decrement" onClick={this.decrementAmount}>–</span>
        <div className="input-number input-number-1"  >{this.state.amount}</div>
        <span className="input-number-increment" onClick={this.incrementAmount}>+</span>
    </div>
    <div className="item__price-2">
    {this.props.price} сум
    </div>
    <button className="delete" onClick={this.delete}>
    <img src="/img/delete.svg" width="30" />
    </button>
</div>
</MediaQuery>
<MediaQuery maxWidth={768}>
    <div className="restaurant__ins-2">
        <div className="restaurant__text">
            <img src={this.props.food.image} width="50" className="vm" />
            <span className="dots-lil">{this.props.food.name_ru}</span>
            <div style={{paddingTop:'10px'}} className={this.props.size.length > 0 ? '' : 'display-none' }>Вид :
            {this.props.size.map((item, i) => (
                <React.Fragment key={item.id}>
                    {i === 1 ? ','+item.name+ ' ':' '+item.name}
                </React.Fragment>
            )) }
            </div>
            <div style={{paddingTop:'10px'}} className={this.props.ids.length > 0  ? '' : 'display-none' }>Добавки :
            {this.props.ids.map((item, i) => (
                <React.Fragment key={item.id}>
                    {i === 1 ? ','+item.name+ ' ':' '+item.name}
                </React.Fragment>
            )) }
            </div>
        <button className="delete" onClick={this.delete}>
            <img src="/img/delete.svg" />
        </button>
        </div>
        <div className="restaurant__p">
            {this.props.food.desc_ru}
        </div>
        <div className="restaurant__price-2">
            {this.props.price} сум
        </div>
        <div className="restaurant__num">
            <span className="input-number-decrement" onClick={this.decrementAmount}>–</span>
            <div className="input-number input-number-1">{this.state.amount}</div>
            <span className="input-number-increment" onClick={this.incrementAmount}>+</span>
        </div>
    </div>
</MediaQuery>
            </div>
        );
    }
}
