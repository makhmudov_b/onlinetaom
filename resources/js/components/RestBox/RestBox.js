import React, { Component } from 'react';

export default class RestBox extends Component {
    render() {
        return (
            <div className="restaurant__ins--desktop">
                <img src="img/mobile1.png" className="restaurant__img" />
                <div className="restaurant__event">
                    <img src="img/brand1.png" width="70" className="restaurant__brand" />
                    Закажи на сумму свыше 60 000 сум и получи подарок!
                </div>
                <div className="restaurant__line"></div>
                <div className="restaurant__ins-p">
                    <h5 className="restaurant__title">Caffee’issimo</h5>
                    <p className="restaurant__desc">Европейская</p>
                </div>
                <div className="restaurant__ins-price">
                <div className="restaurant__price"><img src="img/popularmob.svg" height="10" /> 12 000 сум</div> 
                    <p className="restaurant__long-desktop--event">4.2 км</p>
                </div>           
            </div>
        );
    }
}