import React, { Component } from 'react';
import axios from "axios/index";

export default class BasketItemMobile extends Component {
    delete = () => {
        let axiosConfig = {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios
            .delete(`https://onlinetaom.uz/api/order/${this.props.id}`, axiosConfig)
            .then(result => {
                this.props.deleted();
            })
            .catch((err) => {
                console.log(err);
            })
    }
    render() {
        return (
<div className="restaurant__ins-2">
      <div className="restaurant__text">
      <img src={this.props.order_food[0].food.image} width="50" className="vm" /> {this.props.order_food[0].food.name_ru}
      <button className="delete" onClick={this.delete}>
      <img src="/img/delete.svg" />
      </button>
      </div>
      <div className="restaurant__p">
          {this.props.order_food[0].food.desc_ru}
      </div>
      <div className="restaurant__price-2">
          {this.props.price} сум
      </div>
      <div className="restaurant__num">
      <span className="input-number-decrement">–</span><input className="input-number input-number-1" type="text" defaultValue="1"
            min="1" max="10" /><span className="input-number-increment">+</span>
      </div>
</div>
        );
    }
}
