import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import axios from "axios";

class Finder extends Component {
    _keyword = null;
    constructor(props){
        super(props);

        this.state ={
            redirect : false,
        };
    }


    componentWillReceiveProps(nextProps, nextContext) {
        if(nextProps.finder){
            this.focus();
        }
        if(nextProps.searchData !== null){
            this.setState({
                redirect : false
            })
        }
    }

    onChange = (e) => {
        this._keyword = e.target.value;
    }

    handleKeyDown = (e) => {
        if(this.props.finder){
            if (e.keyCode === 27) {
                this.props.toggle();
            }
        }
        if (e.key === 'Enter') {
            this.search()
            this.props.toggle()
            this.props.history.push('/search')
        }
    }
    search = () => {
        let data = {
            keyword: this._keyword
        };
        axios.patch("https://onlinetaom.uz/api/search", data)
            .then(result => {
                this.props.runSearch(result.data, this._keyword)
                }
            )
            .catch((err) => {
                console.log(err);
            });
    }
    focus = () =>{
        this.autofocus.focus();
    }
    render() {
        return (
        <div className={`finder search-result ${this.props.finder ? 'front-index' : 'display-none md-hide'}`} style={{borderBottom:'none'}}>
            <div className="container relative">
                <label htmlFor="search-res">
                    <img src="/img/searchgreen.svg" />
                </label>
                <input type="text" name="search" id="search-res" onChange={this.onChange} onKeyDown={this.handleKeyDown} ref={(input) => { this.autofocus = input; }} />
                <div className="delete pointer" onClick={this.props.toggle}>
                    <img src="/img/delete.svg" />
                </div>
            </div>
        </div>
        );
    }
}

export default withRouter(Finder)
