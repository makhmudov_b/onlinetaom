import React, { Component } from 'react';
import { Link } from "react-router-dom";

export default class RestInnerBox extends Component {
    render() {
        return (
            <Link to={`/restaurant/${this.props.alias}/${this.props.id}`} className="inner-choice__wrapper block">
                <img src={this.props.image} className="inner-choice--img" />
                <div className="inner-choice--text">
                    <div className="inner-choice__title dots-lil">
                    { this.props.name_ru }
                    </div>
                    <div className="inner-choice__price">
                    { this.props.price } сум
                    </div>
                </div>
                <button className="inner-choice__btn">
                    <img src="/img/add.svg" />                                        
                </button>
            </Link>
        );
    }
}
