import React, { Component } from 'react';

export default class Location extends Component {
    render() {
        return (
<div className="mobnav__bottom mobile text-center">
    <div className="text-center" style={{color: "#144E00"}}>
        {this.props.address}
    <button className="mobnav__btn" onClick={this.props.open}><img src="/img/location.svg" /></button>
    </div>
</div>
        );
    }
}
