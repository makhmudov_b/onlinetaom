import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import RestInnerBox from '../../components/RestInnerBox'
import RestInnerBoxMobile from '../../components/RestInnerBoxMobile'
import axios from 'axios';
import Modal from 'react-modal';
import DesktopModal from '../../components/DesktopModal'
import Slider from "react-slick";
import Loader from 'react-loader-spinner';

Modal.setAppElement(document.getElementById('modal'))
const customStyles = {
    content : {
        top                   : '0%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : '0',
        transform             : 'translate(-50%, 0%)',
        position              : 'fixed',
        overflowY             : 'auto',
        overflowX             : 'hidden',
        padding               : 0,
        background            : 'transparent',
        border                : 'none',
        borderRadius          : '0',
    }
};

export default class RestInner extends Component {
    _isMounted = false;

    constructor(props){
        super(props);

        this.state={
            isLoaded: false,
            loading: false,
            restaurant: null,
            distance: null,
            day: null,
            favorite: false,
            favoriteId: null,
            error: null,
            isActive: 0,
            long: null,
            lat: null,
            modalIsOpen: false,
            modalData : null,
            params: this.props.match.params ,
        };
    }

    openModal = () => {
        this.setState({modalIsOpen: true});
    }

    closeModal = () => {
        this.setState({modalIsOpen: false});
    }


    componentDidMount() {
        this._isMounted = true;
        window.scrollTo(0, 0);
        let now = new Date();
        let days = [6,0,1,2,3,4,5];
        let day = days[ now.getDay() ];
        axios
            .get(`https://onlinetaom.uz/api/restaurant/${this.state.params.alias}`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(result => {
                if (this._isMounted) {
                    this.setState({
                            isLoaded: true,
                            restaurant: result.data,
                            day: day,
                            long: this.props.long,
                            lat: this.props.lat,
                        },
                        this.getFavorite
                    );
                }
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    getDistance = () => {
        let destination = (JSON.parse(localStorage.getItem('location')));
        let data = {
            origin: this.state.restaurant.lat + ',' + this.state.restaurant.long,
            destination: destination.lat + ',' + destination.long
        };
        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.post("https://onlinetaom.uz/api/distance", data, headers)
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        distance: result.data,
                    }));
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
    getFavorite = () => {
        if(localStorage.getItem('token') !== null ){
            axios
                .get(`https://onlinetaom.uz/api/favorite/${this.state.restaurant.id}`,
                    {
                        headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                    })
                .then(result => {
                        if(Object.keys(result.data).length === 0){
                            this.setState(prevState => ({
                                ...prevState,
                                favorite: false,
                            }));
                        }
                        else {
                            this.setState(prevState => ({
                                ...prevState,
                                favorite: true,
                                favoriteId: result.data[0].id
                            }))
                        }
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        }
        if(localStorage.getItem('restaurant'+this.state.restaurant.id) == null){
            this.getDistance()
        }
        else{
            let distance = JSON.parse(localStorage.getItem('restaurant'+this.state.restaurant.id)).distance
            this.setState(prevState => ({
                ...prevState,
                distance: distance,
            }));
        }
    }

    chooseFavorite = () => {
        if(localStorage.getItem('token') !== null ) {
            let data = {
                type: 'restaurant',
                id: this.state.restaurant.id
            };
            let headers = {
                headers : {
                    'Authorization': `Bearer ${localStorage.getItem('token')} }`
                }
            };
            axios.post("https://onlinetaom.uz/api/favorite", data, headers)

                .then(result => {
                        if(result.data){
                            this.setState(prevState => ({
                                ...prevState,
                                favorite: true,
                                favoriteId: result.data
                            }));
                        }
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        }
    }
    deleteFavorite = () => {
        axios
            .delete(`https://onlinetaom.uz/api/favorite/${this.state.favoriteId}`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        favorite: false
                    }));
                }
            );
    }
    chooseCategory = (event) => {
        let id = parseInt(event.target.id);
        if(id !== 0){
            axios
                .get(`https://onlinetaom.uz/api/restaurant/${this.state.params.alias}`,
                    {
                        headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                    })
                .then(result => {
                        this.setState(prevState => ({
                                ...prevState,
                                isActive: id,
                                restaurant: {
                                    ...prevState.restaurant,
                                    foods: result.data.foods.filter(el => {
                                        return el.category_id == id;
                                    })
                                }
                            }
                        ));
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        }
        else{
            axios
                .get(`https://onlinetaom.uz/api/restaurant/${this.state.params.alias}`,
                    {
                        headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                    })
                .then(result => {
                        this.setState(prevState => ({
                                ...prevState,
                                isActive: 0,
                                restaurant: {
                                    ...prevState.restaurant,
                                    foods: result.data.foods
                                }
                            }
                        ));
                    },
                    (error) => {
                        this.setState({
                            isLoaded: true,
                            error
                        });
                    }
                );
        }
    }
    handleClick = (e) => {
        this.setState(prevState => ({
            ...prevState,
            loading: true,
        }));
        axios
            .get(`https://onlinetaom.uz/api/restaurant/${this.state.params.alias}/${e.currentTarget.id}`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        loading: false,
                        modalData: result.data
                    }))
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    update = (item,price) => {
        this.props.updateBasket(item , price);
    }
    render() {
        const settings = {
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            dots:false,
        };
        if(this.state.lat === 0){
            return (<Redirect to="/" />);
        }
        if(this.state.restaurant){
            return (
                <div>
                    <Modal id="modal" ariaHideApp={false}
                           isOpen={this.state.modalIsOpen}
                           onRequestClose={this.closeModal}
                           style={customStyles}
                           overlayClassName="modal-bg"
                    >
                        <DesktopModal notify={this.props.notify} toggle={this.props.togglePopup} restId={this.state.restaurant.id} check={this.props.restId}  close={this.closeModal} desktopUpdate={this.props.update} loading={this.state.loading} information={ this.state.modalData } />
                    </Modal>
                    <section className="favorite-rest desktop">`
                        <div className="container cont-fix flex pt-40 pb-40" >
                            <div className="flex__bottom">
                                <img src={this.state.restaurant.image} width="80" className="vm" />
                                <div className="favorite-rest__text" style={{paddingLeft:'20px'}}>
                                    <span className="favorite-rest__text-title">{this.state.restaurant.name}</span>
                                    <p className="favorite-rest__text-p">{this.state.restaurant.categories.length > 0 ? this.state.restaurant.categories[0].name_ru : ""}</p>
                                </div>
                                <button className={`favorite-rest__btn ${localStorage.getItem('token') !== null ? '': 'none'}`} onClick={!this.state.favorite ? this.chooseFavorite : this.deleteFavorite }>
                                    <img src={!this.state.favorite ? `/img/favorite.svg` : `/img/activefav.svg`} />
                                </button>
                            </div>
                            <div className="flex__bottom">
                                <div className="favorite-rest__info">
                                    {this.state.restaurant.desc_ru}
                                </div>
                            </div>
                            <div className="flex__bottom flex-auto">
                                <div className="favorite-rest__el">
                                    <div className="favorite-rest__el-whole">
                                        <div className="favorite-rest__el--btn active">
                                            <img src="/img/truck.svg" />
                                        </div>
                                        <div className="favorite-rest__el--info active-bold">
                                            {this.state.distance ? `${this.state.distance.price} сум` : 'Вычесление ...'}
                                        </div>
                                    </div>
                                    <div className="favorite-rest__el-whole">
                                        <div className="favorite-rest__el--btn">
                                            <img src="/img/longitude.svg" />
                                        </div>
                                        <div className="favorite-rest__el--info">
                                            {this.state.distance ? this.state.distance.distance : 'Вычесление ...'}
                                        </div>
                                    </div>
                                    <div className="favorite-rest__el-whole">
                                        <div className="favorite-rest__el--btn">
                                            <img src="/img/time.svg" />
                                        </div>
                                        <div className="favorite-rest__el--info">
                                            {this.state.restaurant.schedules[this.state.day].open_hour.slice(0,-3) } - {this.state.restaurant.schedules[this.state.day].close_hour.slice(0,-3)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                        <div className="favorite desktop">
                            <button className={`favorite__btn-1 ${this.state.isActive === 0 ? 'active' : ''}`} id="0" onClick={this.chooseCategory} >Все</button>
                            {this.state.restaurant.categories.map(item => (
                                <button className={`favorite__btn-1 ${this.state.isActive === item.id ? 'active' : ''}`} id={item.id} onClick={this.chooseCategory} key={item.id}>{item.name_ru}</button>
                            ))}
                        </div>
                    <section className="food desktop">
                        <div className="container cont-fix">
                            <div className="flex">
                            {this.state.restaurant.foods.map(item => (
                                <RestInnerBox alias={this.state.restaurant.alias} { ...item } key={item.id} onClick={() => this.openModal()} getId={(e) => this.handleClick(e)} />
                            ))}
                            </div>
                        </div>
                    </section>
                    <div className="inner mobile" style={{background: `url(${this.state.restaurant.cover}) center no-repeat`,backgroundSize: 'cover' }}>
                        <div className="container">
                            <button className={`inner__fav ${localStorage.getItem('token') !== null ? '': 'none'}`}onClick={!this.state.favorite ? this.chooseFavorite : this.deleteFavorite }>
                                <img src={!this.state.favorite ? `/img/favorite.svg` : `/img/activefav.svg`} />
                            </button>
                            <img src={this.state.restaurant.image} width="140" className="inner__brand br50" />
                        </div>
                    </div>
                    <div className="inner-info mobile">
                        <div className="container cont-mobile pt-40">
                            <h4 className="inner-info__title">{this.state.restaurant.name}</h4>
                            <p className="inner-info__desc">{this.state.restaurant.categories.length>0?this.state.restaurant.categories[0].name_ru:""}</p>
                            <div className="col-xs-6">
                                <ul className="inner-info__ul">
                                    <li className="active"><button className="inner-info__btn"><img src="/img/inner1.svg" /></button> {this.state.distance ? `${this.state.distance.price} сум` : 'Вычесление ...'}</li>
                                    <li><button className="inner-info__btn"><img src="/img/inner2.svg" /></button> {this.state.distance ? this.state.distance.distance : 'Вычесление ...'} </li>
                                    <li><button className="inner-info__btn"><img src="/img/inner3.svg" /></button> {this.state.restaurant.schedules[this.state.day].open_hour.slice(0,-3) } - {this.state.restaurant.schedules[this.state.day].close_hour.slice(0,-3)}</li>
                                </ul>
                            </div>
                            <div className="col-xs-6">
                                <div className="inner-info__p">
                                    {this.state.restaurant.desc_ru}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="rest-list mobile" style={{overflowX:'scroll',whiteSpace:'nowrap'}}>
                        <div className={`rest-list__inner ${this.state.isActive === 0 ? 'active' : ''}`} style={{display:'inline-block',width:'120px'}} id="0" onClick={this.chooseCategory} >Все</div>
                        {this.state.restaurant.categories.map(item => (
                            <div className={`rest-list__inner ${this.state.isActive === item.id ? 'active' : ''}`} style={{display:'inline-block',width:'120px'}} id={item.id} onClick={this.chooseCategory} key={item.id}>{item.name_ru}</div>
                        ))}
                    </div>
                    <div className="inner-choice mobile">
                        {this.state.restaurant.foods.map(item => (
                            <RestInnerBoxMobile { ...item } alias={this.state.params.alias} key={item.id} />
                        ))}
                    </div>
                </div>
            );
        }else{
            return <div className="text-center">
                <Loader type="Ball-Triangle" color="#C2F62C" height="100" width="100"/>
            </div>;
        }
    }
}
