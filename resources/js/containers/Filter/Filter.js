import React, { Component } from 'react';
import { Link , withRouter } from "react-router-dom";

import axios from "axios";


class Filter extends Component {
    _keyword = null;
    constructor(props){
        super(props);

        this.state ={
            redirect : false,
        };
    }

    handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            this.search()
            this.props.history.push('/search')
            this.props.toggleMenu()
        }
    }
    search = () => {
        let data = {
            keyword: this._keyword
        };
        axios.patch("https://onlinetaom.uz/api/search", data)
            .then(result => {
                this.props.runSearch(result.data, this._keyword)
                }
            )
            .catch((err) => {
                console.log(err);
            });
    }
    onChange = (e) => {
        this._keyword = e.target.value;
    }

    render() {
        return (
        <div>
        <div className="boxshadow pt-20 pb-20">
            <div className="col-xs-3">
                <div id="find">
                    <span className="search-icon"></span>
                    <input type="text" id="find-input" onChange={this.onChange} onKeyDown={this.handleKeyDown} />
                </div>
            </div>
            <div className="col-xs-6 text-center">
            </div>
            <div className="col-xs-3 text-right">
                <button id="menu">
                    <img src="/img/hamb.svg" className="mobnav__hamburger" id="open" />
                </button>
            </div>
            <div className="clearfix"></div>
        </div>
        <div className="mobnav__item" onClick={this.props.toggleMenu}>
            <Link to="/profile">
                <img src="/img/account.svg" width="35" /> Мой профиль
            </Link>
        </div>
        <div className="mobnav__item" onClick={this.props.toggleMenu}>
            <Link to="/events">
                <img src="/img/event.svg" width="35" /> Акции
            </Link>
        </div>
        <div className="mobnav__item" onClick={this.props.toggleMenu}>
            <Link to="/favorites">
                <img src="/img/favorite-nav.svg" width="35" /> Избранное
            </Link>
        </div>
        <div className="mobnav__item">
            <span className="nav__number-2">998 71 200-7-200</span>
            <span className="nav__supp-2">поддержка</span>
        </div>
        <div className="mobnav__bot">
            <button className="useful__btn-2"><img src="/img/playmarket.svg" />Загрузить для Android</button>
            <button className="useful__btn-2"><img src="/img/appstore.svg" />Загрузить для ApPlE</button>
            <a>О сервисе</a>
            <a>Стать курьером</a>
            <a>Пользовательское соглашение</a>
        </div>
        </div>
        );
    }
}

export default withRouter(Filter)
