import React,  { Component } from 'react';
import Search from '../../components/Search'
import MainSlider from '../../components/MainSlider'
import Type from '../../components/Type'
import Popular from '../../components/Popular'
import Download from '../../components/Download'
import Text from '../../components/Text'


export default class Home extends Component {
    constructor(props) {
        super(props);
        this.home = React.createRef();

        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            // modalIsOpen: !this.props.cart.ready,
        }
    }



    componentDidMount(){
        window.scrollTo(0, 0);
    }

    render() {
        return (
        <div>
            <Search { ... this.props.cart } open={this.props.openModal} />
            <MainSlider />
            <Type />
            <Popular longitude={this.props.cart.longitude} ready={this.props.cart.ready} latitude={this.props.cart.latitude} />
            <Download />
            <Text />
        </div>
        );
    }
}
