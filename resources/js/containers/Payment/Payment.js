import React, { Component } from 'react';
import axios from "axios";

export default class Payment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoaded: false,
            basket: null,
            error: null,
            distance : null,
            totalPrice : 0,
            isActive : 'fast'
        };
    }
    componentDidMount() {
      window.scrollTo(0, 0);
        axios
            .get(`https://onlinetaom.uz/api/checkout/${this.props.match.params.id}`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(result => {
                    this.setState({
                        basket: result.data
                    });
                },
                (error) => {
                    console.warn(error)
                }
            );
    }
    render() {
        if(this.state.basket !== null){
        return (
        <div>
        <div className="events mobile">
            <div className="container">
                <div className="text-center">
                    Статус заказа
                </div>
            </div>
        </div>
        <div className="method pt-20">
            <div className="container cont-fix d-flex">
                <div className="proccess__element" style={{marginRight: '8px'}}>
                    <div className="proccess__text">
                        Текущий статус
                    </div>
                    <div className="proccess__status">
                        <div className="progress-container">
                            {this.state.basket.payment_method === '1' && this.state.basket.is_paid === 1 ? 'Вы не совершили оплату поэтому ваш заказ аннулирован' : ''}
                            {this.state.basket.payment_method === '1' && this.state.basket.is_paid === 3 ? 'Вы не совершили оплату поэтому ваш заказ аннулирован' : ''}
                            <ol className={`progression-bar step-${parseInt(this.state.basket.status) } ${this.state.basket.payment_method === '1' && this.state.basket.is_paid === 1 ? 'display-none' : ''}`}>
                               <li className="is-active"><span className="progression-date">Оформлен </span></li>
                               <li className={this.state.basket.status >= 2 ? 'is-active' : ''}><span className={`progression-date`}>Принят</span></li>
                               <li className={this.state.basket.status >= 3 ? 'is-active' : ''}><span className={`progression-date`}>В пути</span></li>
                               <li className={this.state.basket.status >= 4 ? 'is-active' : ''}><span className={`progression-date`}>Доставлено</span></li>
                            </ol>
                            {this.state.basket.payment_method === '1' && this.state.basket.is_paid === 2 ? 'Оплачен' : ''}
                        </div>
                        <div className="proccess__info" style={{paddingTop:'20px'}}>
                            <p>Ресторан: {this.state.basket.restaurant.name} </p>
                            <p>Доставка: {this.state.basket.shipping_time} </p>
                            <p>Форма оплаты: {this.state.basket.payment_method == 0 ? 'Наличные' : 'Payme'} </p>
                            {this.state.basket.comment ? <p>Комментарий: {this.state.basket.comment}</p> : null}
                        </div>
                    </div>
                    <div className="proccess__address">
                        <div className="proccess__address--title">
                            Адрес доставки
                        </div>
                        <div className="proccess__address--text">
                            {this.state.basket.adress}
                        </div>
                    </div>
                </div>

                <div className="proccess__element">
                    <div className="proccess__text" style={{paddingBottom:'9px'}}>
                        Мой заказ #{this.state.basket ? this.state.basket.id : null}
                    </div>
                    <div className="proccess__item">
                    {this.state.basket !== null ? this.state.basket.order_food.map(item => (
                    <div className="restaurant__ins-2" key={item.id} style={{width : '100%', paddingBottom: '5px'}}>
                        <div className="restaurant__text">
                            <img src={item.food.image} width="50" style={{ height: '50px' , objectFit: 'cover' }} className="vm br50" />
                            <span className="dots-big vm" style={{paddingLeft: '15px'}}>{item.food.name_ru}
                                <span style={{paddingTop: '5px',display:'block',fontWeight:'300'}}>{item.food.price} сум</span>
                            </span>
                            <span className="absolute" style={{right: '10px',top:'50%'}}>{item.amount} шт</span>
                        </div>
                    </div>
                    )) : ''}
                    </div>
                    <div className="proccess__price">
                        <div className="proccess__price--item">
                            Доставка:   <span>{this.state.basket ? this.state.basket.shipping_price + ' сум' : ''}</span>
                        </div>
                        <div className="proccess__price--item">
                            Стоимость заказа:   <span>{this.state.basket ? parseInt(this.state.basket.total_price - this.state.basket.shipping_price) + ' сум' : ''} </span>
                        </div>
                        <div className="proccess__price--item">
                            Общая стоимость:   <span>{this.state.basket ? this.state.basket.total_price + ' сум' : ''}</span>
                        </div>
                        <div className="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        );
        }
        else{
            return null;
        }
    }
}
