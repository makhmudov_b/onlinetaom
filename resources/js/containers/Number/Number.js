import React, { Component } from 'react';
import Search from '../../components/Search'
import MainSlider from '../../components/MainSlider'
import Type from '../../components/Type'
import Popular from '../../components/Popular'
import Download from '../../components/Download'
import Text from '../../components/Text'

export default class Number extends Component {
    render() {
        return (
        <div>
        <div className="events mobile">
            <div className="container">
                <button className="events__arrow">
                    <img src="/img/arrowgreen.svg" />
                </button>
                <div className="text-center">
                    Вход
                </div>
            </div>
        </div>
        <div className="verify">
            <div className="text-center">
                <img src="/img/phone.svg" className="verify__phone" />
                <div className="verify__sms">
                    Мой номер
                </div>
            </div>
            <div className="verify__input">
               <label htmlFor="tel">+998</label><input type="tel" id="tel" />
            </div>
            <div className="verify__about">
                    Нажимая на кнопку “Получить код”, <a href="">я соглашаюсь с условиями использования приложения</a>
            </div>
        </div>                       
        </div>
        );
    }
}