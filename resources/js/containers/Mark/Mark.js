import React, { Component } from 'react';
import Search from '../../components/Search'
import MainSlider from '../../components/MainSlider'
import Type from '../../components/Type'
import Popular from '../../components/Popular'
import Download from '../../components/Download'
import Text from '../../components/Text'

export default class Mark extends Component {
    render() {
        return (
        <div>
            <div className="mark mobile">
                <div className="container">
                    <button className="events__arrow">
                        <img src="/img/arrowgreen.svg" />
                    </button>
                </div>
            </div>
            <div className="verify">
                <div className="text-center">
                    <img src="/img/logo.svg" className="mt-30 mb-30" />
                </div>
                <div className="mark__wrap">
                    <div className="mark__text">
                        Оцените нас, пожалуйста
                        <form className="rating mark__stars">
                            <input type="radio" id="star5" name="rating" value="5" /><label className="full" htmlFor="star5"></label>
                            <input type="radio" id="star4" name="rating" value="4" /><label className="full" htmlFor="star4"></label>
                            <input type="radio" id="star3" name="rating" value="3" /><label className="full" htmlFor="star3"></label>
                            <input type="radio" id="star2" name="rating" value="2" /><label className="full" htmlFor="star2"></label>
                            <input type="radio" id="star1" name="rating" value="1" /><label className="full" htmlFor="star1"></label>
                        </form>
                    </div>
                </div>
                <div className="mark__wrap">
                    <div className="mark__add">
                        <input type="radio" name="same" id="fast1" />
                        <label htmlFor="fast1">Bad</label>
                        <input type="radio" name="same" id="fast2" />
                        <label htmlFor="fast2">Ok</label>
                        <input type="radio" name="same" id="fast3" />
                        <label htmlFor="fast3">Good</label>
                        <br /><br />
                        <input type="text" placeholder="Добавьте отзыв" />
                    </div>
                </div>
                <div className="mark__info">
                        Помогите нам повысить качество обслуживания и всегда поддерживать его на высоком уровне!                
                </div>
            </div>                                     
        </div>
        );
    }
}