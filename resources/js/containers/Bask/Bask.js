import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import Basket from '../../components/Basket'
import Check from '../../components/Check'
import axios from "axios";

export default class Bask extends Component {
    constructor(props){
        super(props);

        this.state={
            redirect: 'basket',
            basket: null,
            distance: null,
            totalPrice : 0,
            comment : '',
            time : 'Быстрая доставка',
            moveToOrder: false
        }
    }
    componentWillMount() {
        axios
            .get(`https://onlinetaom.uz/api/order`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                        basket: result.data
                    }),
                        this.getDistance(result.data)
                    );
                },
                (error) => {
                    console.warn(error)
                }
            )
    }

    componentDidMount() {
      window.scrollTo(0, 0)
    }

    getDistance = (datas) => {
        if(datas.restaurant_id !== 0){
        if(datas.restaurant_id != null){
        let destination = (JSON.parse(localStorage.getItem('location')));
        let data = {
            restaurantId: datas.restaurant.id,
            origin: datas.restaurant.lat + ',' + datas.restaurant.long,
            destination: destination.lat + ',' + destination.long
        };
        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.post("https://onlinetaom.uz/api/distance", data, headers)
            .then(result => {
                    this.setState(prevState => ({
                        ...prevState,
                       distance: result.data,
                    })
                        )
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
        }
        }
    }
    // getFullPrice = () => {
    //     let price = parseInt((this.state.distance.price).replace(/ /g,''));
    //     if(this.props.price !== null){
    //         this.setState(prevState => ({
    //             ...prevState,
    //             totalPrice: this.props.price + price
    //         }));
    //     }
    // }
    addComment = (commented) => {
        this.setState(prevState => ({
            ...prevState,
            comment: commented
        }));
    }
    changeTime = (time) => {
        this.setState(prevState => ({
            ...prevState,
            time: time
        }));
    }
    checkout = (method) => {
        let destination = (JSON.parse(localStorage.getItem('location')));
        let {price, address} = this.props;
        let { distance ,time , comment} = this.state;
        let data = {
            payment_method: method,
            shipping_time : time,
            shipping_price : parseInt((distance.price).replace(/ /g,'')),
            distance : distance.distance,
            adress : address,
            long : destination.long,
            lat : destination.lat,
            comment : comment,
            total_price : parseInt(price) + parseInt((distance.price).replace(/ /g,''))
        };
        let headers = {
            headers : {
                'Authorization': `Bearer ${localStorage.getItem('token')} }`
            }
        };
        axios.put(`https://onlinetaom.uz/api/checkout/${this.state.basket.id}`, data, headers)
        .then(result => {
                this.props.update()
                if(result.data.url != ''){
                  window.location = result.data.url;
                } else{
                this.setState(prevState => ({
                    ...prevState,
                    moveToOrder : true
                }))
                }
            },
            (error) => {
                console.warn(error)
            }
        );
    }
    updated = () => {
        axios
            .get(`https://onlinetaom.uz/api/order`,
                {headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`} })
            .then(result => {
                    this.setState({
                        isLoaded: true,
                        basket: result.data
                    });
                    this.props.update();
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    forwardTo = (auth) => {
        this.setState(prevState => ({
            ...prevState,
            redirect: auth
        }))
    }

    render() {
        if(localStorage.getItem('token') !== null){
            if(this.state.moveToOrder){
                return (<Redirect to={`/order/${this.state.basket.id}`} />)
            }
            if(this.state.redirect === 'basket'){
                return (
                <div>
                    <Basket notify={this.props.notify} toggleMap={this.props.toggleMap} long={this.props.long} lat={this.props.lat} getDistance={this.getDistance} addComment={(commented) => this.addComment(commented)} changeTime={(time) => this.changeTime(time)} updated={this.updated} { ... this.state  } forwardTo={this.forwardTo} updateItem={this.props.update} address={this.props.address} price={this.props.price} />
                </div>
                );
            }
            if(this.state.redirect === 'check'){
                return (
                <div>
                <Check { ... this.state  } checkout={this.checkout} address={this.props.address} forwardTo={this.forwardTo} price={this.props.price} />
                </div>
                );
            }
        }
        else{
            return (<Redirect to={'/auth'} />)
        }
    }
}
