import React, { Component } from 'react';
import MainSlider from '../../components/MainSlider'
import FavoriteFood from '../../components/FavoriteFood'
import axios from 'axios';

export default class Event extends Component {
    constructor(props){
        super(props);
        this.state ={
            event : null,
        };
    }
    componentDidMount() {
      window.scrollTo(0, 0)
            axios
            .get(`https://onlinetaom.uz/api/events`,
                {
                    headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
                })
            .then(result => {
                    this.setState({
                        event: result.data
                    },
                    this.getFavorite
                    );
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            );
    }
    render() {
        if(this.state.event){
        return (
        <div>
            <MainSlider />
            <div className="restaurant">
            <div className="container cont-fix">
                <div className="flexbox">
                    {this.state.event.map(item => (
                        <FavoriteFood key={item.id} { ... item } style={{height:'auto'}} />
                    ))}
                </div>
            </div>
            </div>
        </div>
        );
        }
        else{
            return 'loading';
        }
    }
}
