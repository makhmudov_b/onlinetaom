import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import MediaQuery from 'react-responsive';
import InputMask from 'react-input-mask';


export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false,
            user: null,
            redirect: false,
            phone: null,
        };
        this.onChange = this.onChange.bind(this);
    }
    componentDidMount() {
        if(localStorage.getItem('token') !== null){
            this.setState({
                redirect : '/'
            })
        }
      window.scrollTo(0, 0)
    }
    login = () =>{
        fetch("https://onlinetaom.uz/api/check",{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: this.state.phone ,
            })
        })
        .then(response => response.json())
        .then(
            (result) => {
                this.setState({
                isLoggedIn: true,
                user : result
            },
            this.check
            );
        })
        .catch(
            (error) => {
                console.log(error)
        });
    }
    desktopLogin = () => {
        fetch("https://onlinetaom.uz/api/check",{
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                phone: this.state.phone
            })
        })
        .then(response => response.json())
        .then(
            (result) => {
                this.setState({
                isLoggedIn: true,
                user : result
            },
            this.checkDesktop
            );
        })
        .catch(
            (error) => {
                console.log(error)
        });
    }
    check = () => {
        if(this.state.user !== null){
            localStorage.setItem('phone', this.state.phone);
            this.setState({
                redirect: '/verify',
            });
        }
        else{
            console.log('Error')
        }
    }
    checkDesktop = () => {
        if(this.state.user !== null){
            localStorage.setItem('phone', this.state.phone);
            this.props.set('verify');
        }
        else{
            console.log('Error')
        }
    }
    onChange = (e) => {
        this.setState( {
            [e.target.name]: e.target.value.replace(/\s/g, "").replace(/\-/g, "")
        })
    }
    render() {
        if(this.state.redirect === '/verify' || this.state.redirect === '/'){
            return (<Redirect to={this.state.redirect} />)
        }

        return (
            <div className="text-center">
            <div className="popup-white">
                <div className="pl-20">
                    <div className="text-left">
                        Вход
                    </div>
                    <div className="profile__account">
                        <img src="/img/accountgreen.svg" />    
                    </div>                   
                </div>
            </div>
            <div className="verify">
                <div className="text-center">
                    <img src="/img/phone.svg" className="verify__phone" />
                    <div className="verify__sms">
                        Мой номер
                    </div>
                </div>
                <div className="verify__input">
                <label htmlFor="tel">+998</label> 
                <InputMask mask="99 999-99-99" type="tel" name="phone" id="tel" onChange={this.onChange} />
                </div>
                <div className="verify__about">
                    Нажимая на кнопку “Получить код”, <a href="">я соглашаюсь с условиями использования приложения</a>
                </div>
                <MediaQuery maxWidth={480}>
                    <button className="verify__btn" onClick={this.login}>Получить код</button>
                </MediaQuery>
                <MediaQuery minWidth={480}>
                    <button className="verify__btn" onClick={this.desktopLogin}>Получить код</button>
                </MediaQuery>
            </div>
        </div>
        );
    }
}
