import React, { Component } from 'react';
import axios from 'axios';

export default class Info extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email : null,
            name : null,
            adress : null,
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0)
        axios
        .get(`https://onlinetaom.uz/api/user`,
            {
                headers: {'Authorization': `Bearer ${localStorage.getItem('token')} }`}
            })
        .then(result => {
            this.setState({
                email : result.data.email,
                name : result.data.name,
                adress: result.data.adress,
            })
            },
            (error) => {
            console.warn(error);
            }
        );
    }
    edit = () => {
    let data = {
        email : this.state.email,
        name : this.state.name,
        adress : this.state.adress,
    };
    let headers = {
        headers : {
            'Authorization': `Bearer ${localStorage.getItem('token')} }`
        }
    };
        axios
        .put(`https://onlinetaom.uz/api/user/edit`,
            data , headers )
        .then(result => {
            if(result.data){
                this.props.notify('success');
            }
            },
            (error) => {
            console.warn(error);
            }
        );
    }
    eventHandler = (e) => {
        let _naming = e.target.name;
        let value = e.target.value;
        this.setState( ({
            [_naming]: value
        })
    );
    }
    
    render() {
        if(this.state.name || this.state.adress || this.state.email){
        return (
        <div>
        <div className="events">
            <div className="container cont-fix">
                <div className="text-center">
                    Личные данные
                </div>
            </div>
        </div>
        <div className="info">
            <div className="container cont-fix">
            <div className="info__inner">
                <div className="info__inner--title">Имя</div>
                <input className="info__inner--show" style={{border:'none'}} name="name" onChange={this.eventHandler} defaultValue={this.state.name ? this.state.name : ''} />
            </div>
            <div className="info__inner">
                <div className="info__inner--title">Адрес доставки</div>
                <input className="info__inner--show" style={{border:'none'}} name="adress" onChange={this.eventHandler} defaultValue={this.state.adress ? this.state.adress : ''} />
            </div>
            <div className="info__inner">
                <div className="info__inner--title">E-mail</div>
                <input type="email" className="info__inner--show" style={{border:'none'}} name="email" onChange={this.eventHandler} defaultValue={this.state.email ? this.state.email : ''} />
            </div>
            <button className="mt-20 basket__btn" onClick={this.edit}>Изменить</button>
            </div>
        </div>
        </div>
        );
    }
    else{
        return null;
    }
}
}
